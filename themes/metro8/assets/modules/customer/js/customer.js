// document ready
$(function () {
    // initiating data table
    var DataTableInstance = new DataTableFactory();
    DataTableInstance.init({
        dt_id : '_datatable',
        columns: [
            { data: 'selectRow', name: 'selectRow' },
            { data: 'id', name: 'id' },    
            { data: 'matricule', name: 'matricule'},
            { data: 'user', name: 'user'},
            { data: 'phone', name: 'phone'},
            { data: 'address', name: 'address'},
            {data: 'created_at', name: 'created_at' ,render: function(data, type, row){
                return new Date(row.created_at).toLocaleDateString();
            }},
            
            { data: 'action', name: 'action' },
        ],
    });
    // initiating modal form management
    var ManageModalFormInstance = new ManageModalForm();
    ManageModalFormInstance.init(DataTableInstance,{
        'first_name': {
            validators: {
                notEmpty: {
                    message: 'Prénom requis'
                }
            }
        },
        'last_name': {
            validators: {
                notEmpty: {
                    message: 'Nom requis'
                }
            }
        },

        'email': {
            validators: {
                notEmpty: {
                    message: 'Email requis'
                }
            }
        },



        'phone': {
            validators: {
                notEmpty: {
                    message: 'numéro de téléphone requis'
                }
            }
        },

        'birthday': {
            validators: {
                notEmpty: {
                    message: 'Date de naissance requis'
                }
            }
        },


        'address': {
            validators: {
                notEmpty: {
                    message: 'Adresse requis'
                }
            }
        },
        'state': {
            validators: {
                notEmpty: {
                    message: 'wilaya requis'
                }
            }
        },
        'circle': {
            validators: {
                notEmpty: {
                    message: 'Daïra requis'
                }
            }
        },

        'zip_code': {
            validators: {
                notEmpty: {
                    message: 'Code postal requis'
                }
            }
        },
    });
});