$(function () {
    //handle module buttons actions
    handleModuleActions();
    // initiating modal form management
    var ManageModalFormInstance = new ManageModalForm();
    ManageModalFormInstance.init(DataTableInstance,{
        'name': {
            validators: {
                notEmpty: {
                    message: 'Customer name is required'
                }
            }
        },
        'phone': {
            validators: {
                notEmpty: {
                    message: 'Phone number is required'
                },
                phone: {
                    message: 'The value is not a valid phone number',
                }
            }
        },
        'email': {
            validators: {
                notEmpty: {
                    message: 'email adress is required'
                },
                emailAddress: {
                    message: 'The input is not a valid email address'
                }
            }
        },
        'password': {
            validators: {
                notEmpty: {
                    message: 'the password is required'
                },
                stringLength: {
                    min: 8,
                    message: 'The password must have at least 8 characters'
                },
            }
        },
    });
});