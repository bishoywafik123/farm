$(function(){

    
    // get token device firebase
	  var firebaseConfig = {
        apiKey: "AIzaSyDXVb4Ykl2rQlhstgAhOeDNloF9m7bm2xU",
        authDomain: "cloa-oran.firebaseapp.com",
        projectId: "cloa-oran",
        storageBucket: "cloa-oran.appspot.com",
        messagingSenderId: "94867917399",
        appId: "1:94867917399:web:69eae82f898422c1ffb112",
        measurementId: "G-Y70K8LW8JY",
        databaseURL: "https://cloa-oran-default-rtdb.firebaseio.com/"
    
          };
          // Initialize Firebase
          firebase.initializeApp(firebaseConfig);
        
            const messaging = firebase.messaging();
          
            function initFirebaseMessagingRegistration() {
                    messaging
                    .requestPermission()
                    .then(function () {
                        return messaging.getToken();
                    })
                    .then(function(token) {
                        console.log(token);
    
                        $('.token_firebase').val(token);
                    
                    }).catch(function (err) {
                        console.log('User Chat Token Error'+ err);
                    });
             }  
        
              initFirebaseMessagingRegistration();
    

              // end








    var ManageFormInstance = new ManageForm();
    ManageFormInstance.init( 'login_form', {
        'email': {
            validators: {
                notEmpty: {
                    message: 'email adress is required'
                },
                emailAddress: {
                    message: 'The input is not a valid email address'
                }
            }
        },
        'password': {
            validators: {
                notEmpty: {
                    message: 'the password is required'
                },
                stringLength: {
                    min: 8,
                    message: 'The password must have at least 6 characters'
                },
            }
        },
    });
});