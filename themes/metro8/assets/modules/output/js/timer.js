// document ready
$(function () {
    // initiating data table
    var DataTableInstance = new DataTableFactory();
    DataTableInstance.init({
        dt_id : '_datatable',
        columns: [
            { data: 'selectRow', name: 'selectRow' },
            { data: 'id', name: 'id' },    
            { data: 'output', name: 'output'},
            { data: 'start_time', name: 'start_time'},
            { data: 'end_time', name: 'end_time'},
            { data: 'status', name: 'status'},
            {data: 'created_at', name: 'created_at' ,render: function(data, type, row){
                return new Date(row.created_at).toLocaleDateString();
            }},
            
            { data: 'action', name: 'action' },
        ],
    });
    // initiating modal form management
    var ManageModalFormInstance = new ManageModalForm();
    ManageModalFormInstance.init(DataTableInstance,{
        'start_time': {
            validators: {
                notEmpty: {
                    message: 'start time is required'
                }
            }
        },

        'end_time': {
            validators: {
                notEmpty: {
                    message: 'end time is required'
                }
            }
        },

        'days': {
            validators: {
                notEmpty: {
                    message: 'day is required'
                }
            }
        },


   
    });


    


});