// document ready
$(function () {

    // initiating data table
    var DataTableInstance = new DataTableFactory();
    DataTableInstance.init({
        dt_id : '_datatable',
        columns: [
            { data: 'selectRow', name: 'selectRow' },
            { data: 'id', name: 'id' },
            { data: 'device', name: 'device' },
            { data: 'tempValue', name: 'tempValue' },
            { data: 'humValue', name: 'humValue' },
            { data: 'soilHumValue', name: 'soilHumValue' },
            { data: 'weatherValue', name: 'weatherValue' },
            { data: 'created_at', name: 'created_at', 
                render: function(data, type, row){
                    return new Date(row.created_at).toLocaleDateString();
                } 
            },
        ],
    });

});