// document ready
$(function () {

    // initiating data table
    var DataTableInstance = new DataTableFactory();
    DataTableInstance.init({
        dt_id : '_datatable',
        columns: [
            { data: 'selectRow', name: 'selectRow' },
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'phone', name: 'phone' },
            { data: 'status', name: 'status' },
            { data: 'price', name: 'price' },
            { data: 'place', name: 'place' },
            { data: 'created_at', name: 'created_at', 
                render: function(data, type, row){
                    return new Date(row.created_at).toLocaleDateString();
                } 
            },
            { data: 'action', name: 'action' },
        ],
    });
    // initiating modal form management
    var ManageModalFormInstance = new ManageModalForm();
    ManageModalFormInstance.init(DataTableInstance,{
        'name': {
            validators: {
                notEmpty: {
                    message: 'name is required'
                }
            }
        },

        'phone': {
            validators: {
                notEmpty: {
                    message: 'phone is required'
                }
            }
        },


        'email': {
            validators: {
                notEmpty: {
                    message: 'email is required'
                }
            }
        },


        'password': {
            validators: {
                notEmpty: {
                    message: 'password is required'
                }
            }
        },



        'place': {
            validators: {
                notEmpty: {
                    message: 'place is required'
                }
            }
        },

        'price': {
            validators: {
                notEmpty: {
                    message: 'price is required'
                }
            }
        },

        'device': {
            validators: {
                notEmpty: {
                    message: 'device is required'
                }
            }
        },
   
    });
});