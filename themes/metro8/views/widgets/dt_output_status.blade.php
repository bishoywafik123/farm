
<div class="custom-control custom-switch custom-switch-md">
<input type="checkbox" class="custom-control-input ajax-status" data-id="{{$id}}" @if($status==1) checked @endif id="customSwitch2">
    <label class="custom-control-label" for="customSwitch2"></label>  
</div>

<script>

    $('.ajax-status').on('click',function(){
            $.ajax({
                url: "{{route('output.status.update')}}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: "POST",
                data: {
                    'id': $(this).data('id')
                },
                success: function (response) {
                    if(DataTableInstance){
                        DataTableInstance.refresh();
                    }
                    displayToastrMessage("success", response.message);
                },
                error: function (response) {
                    if(response.status == 422){
                        displayValidationError(response.responseJSON.errors);
                    }
                }
            });
    
        });
    
    </script>