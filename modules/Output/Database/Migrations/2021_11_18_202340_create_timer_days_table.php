<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimerDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timer_days', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('timer_id');
            $table->foreign('timer_id')->references('id')->on('timers')->onDelete('cascade');
            $table->string('day');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('output_days');
    }
}
