<?php

namespace Modules\Output\Entities;

use Modules\Output\Entities\Output;
use Modules\Output\Entities\TimerDay;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Timer extends Model
{
    use HasFactory;

    protected $fillable = ['start_time','end_time','output_id'];

    public function output()
    {
        return $this->belongsTo(Output::class,'output_id');
    }

    public function days()
    {
        return $this->hasMany(TimerDay::class,'timer_id');
    }

    protected static function newFactory()
    {
        return \Modules\Output\Database\factories\TimerFactory::new();
    }
}
