<?php

namespace Modules\Output\Entities;

use Modules\Output\Entities\Output;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimerDay extends Model
{
    use HasFactory;

    protected $fillable = ['timer_id','day'];

    public function output(){
        return $this->belongsTo(Output::class,'output_id');
    }
    
    protected static function newFactory()
    {
        return \Modules\Output\Database\factories\OutputDayFactory::new();
    }
}
