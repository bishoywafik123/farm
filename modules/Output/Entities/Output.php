<?php

namespace Modules\Output\Entities;

use Modules\Output\Entities\Timer;
use Modules\Device\Entities\Device;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Output extends Model
{
    use HasFactory;

    protected $fillable = ['name','device_id','gpio','state','active'];
    
    public function device(){
        return $this->belongsTo(Device::class,'device_id');
    }

    public function timers()
    {
        return $this->hasMany(Timer::class,'output_id');
    }

    protected static function newFactory()
    {
        return \Modules\Output\Database\factories\OutputFactory::new();
    }
}
