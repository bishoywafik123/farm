<?php

use Modules\Output\Entities\Output;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'dashboard', 'middleware' => 'auth:dashboard'], function(){

    // rest of routes
    Route::group(['prefix' => 'outputs', 'as' => 'output.'], function(){

        //the grouped operations (declare them before resource routes to avoid conflicts)
        Route::delete('delete-many', [Modules\Output\Http\Controllers\OutputController::class, 'destroyMany'])
        ->name('destroy.many')
        ->middleware('can:output.actions.delete');

        Route::get('ajax/datatable', OutputDatatableController::class)
            ->name('datatable')
            ->middleware('can:output.actions.view');

        //Model Resources routes
        //didn't use resource to have the ability to attach permission middleware to each route
        // avoid using middlewares inside of controllers keep this structure maintained
        Route::get('/',[Modules\Output\Http\Controllers\OutputController::class, 'index'])
            ->name('index')
            ->middleware('can:output.actions.view');

        Route::post('/',[Modules\Output\Http\Controllers\OutputController::class, 'store'])
            ->name('store')
            ->middleware('can:output.actions.add');

        Route::get('create',[Modules\Output\Http\Controllers\OutputController::class, 'create'])
            ->name('create')
            ->middleware('can:timer.actions.add');

        Route::put('{output}',[Modules\Output\Http\Controllers\OutputController::class, 'update'])
            ->name('update')
            ->middleware('can:timer.actions.edit');

        Route::get('{output}/edit',[Modules\Output\Http\Controllers\OutputController::class, 'edit'])
            ->name('edit')
            ->middleware('can:timer.actions.edit');

        Route::delete('{output}',[Modules\Output\Http\Controllers\OutputController::class, 'destroy'])
            ->name('delete')
            ->middleware('can:timer.actions.delete');



            
        Route::post('/status/update',[Modules\Output\Http\Controllers\OutputController::class, 'update_status'])
        ->name('status.update');


    });




    
    // rest of routes timer
    Route::group(['prefix' => 'timers', 'as' => 'timer.'], function(){

        //the grouped operations (declare them before resource routes to avoid conflicts)
        Route::delete('delete-many', [Modules\Output\Http\Controllers\TimerController::class, 'destroyMany'])
        ->name('destroy.many')
        ->middleware('can:timer.actions.delete');

        Route::get('ajax/datatable', TimerDatatableController::class)
            ->name('datatable')
            ->middleware('can:timer.actions.view');

        //Model Resources routes
        //didn't use resource to have the ability to attach permission middleware to each route
        // avoid using middlewares inside of controllers keep this structure maintained
        Route::get('/',[Modules\Output\Http\Controllers\TimerController::class, 'index'])
            ->name('index')
            ->middleware('can:timer.actions.view');

        Route::post('/',[Modules\Output\Http\Controllers\TimerController::class, 'store'])
            ->name('store')
            ->middleware('can:timer.actions.add');

        Route::get('create',[Modules\Output\Http\Controllers\TimerController::class, 'create'])
            ->name('create')
            ->middleware('can:timer.actions.add');

        Route::put('{timer}',[Modules\Output\Http\Controllers\TimerController::class, 'update'])
            ->name('update')
            ->middleware('can:timer.actions.edit');

        Route::get('{timer}/edit',[Modules\Output\Http\Controllers\TimerController::class, 'edit'])
            ->name('edit')
            ->middleware('can:timer.actions.edit');

        Route::delete('{timer}',[Modules\Output\Http\Controllers\TimerController::class, 'destroy'])
            ->name('delete')
            ->middleware('can:timer.actions.delete');


    });

});


route::get('/test',function(){

return date('m');

});



