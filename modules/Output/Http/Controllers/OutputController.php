<?php

namespace Modules\Output\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Support\Renderable;
use Modules\Core\Http\Controllers\AppController;
use Modules\Device\Entities\Device;
use Modules\Output\Entities\Output;
use Modules\Output\Http\Requests\OutputRequest;
use Modules\Output\Http\Requests\UpdateOutputRequest;

class OutputController extends AppController
{
    
    public function index()
    {
        $this->setMessages([
            //swal
            'swal-delete-prompt' => Lang::get('core::global.swal.swal-delete-prompt'),
            'swal-delete-prompt-single' => Lang::get('core::global.swal.swal-delete-prompt-single'),
            'swal-hard-delete-prompt' => Lang::get('core::global.swal.swal-hard-delete-prompt'),
            'swal-hard-delete-prompt-single' => Lang::get('core::global.swal.swal-hard-delete-prompt-single'),
            'swal-delete-btn-confirm' => Lang::get('core::global.swal.swal-delete-btn-confirm'),
            'swal-delete-btn-discard' => Lang::get('core::global.swal.swal-delete-btn-discard'),

            'swal-restore-prompt' => Lang::get('core::global.swal.swal-restore-prompt'),
            'swal-restore-prompt-single' => Lang::get('core::global.swal.swal-restore-prompt-single'),
            'swal-restore-btn-confirm' => Lang::get('core::global.swal.swal-restore-btn-confirm'),
            'swal-restore-btn-discard' => Lang::get('core::global.swal.swal-restore-btn-discard'),
        ]);
        return view('output::dashboard.index');
    }


    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Output $output)
    {
        $this->setAjaxParams([
            'dt_modal_request_type' => 'PUT',
            'dt_modal_submit_url' => route('output.update', [$output->id]),
        ]);
        $device=Device::all();
          return view('output::dashboard.modals.edit')->with(['output'=>$output,'device'=>$device]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateOutputRequest $request, Output $output)
    {
       
        if($request->status){
            $status=1;
        }else{
            $status=0;
        }
        $output->update([
            'name'=>$request->name,
            'output_id'=>$request->output,
            'gpio'=>$request->gpio,
            'state'=>$status,
            'active'=>$request->active
            
        ]);
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-updated-row')],200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Output $output)
    {
        $output->delete();
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-deleted-row')], 200);
    }


    public function destroyMany(Request $request){
        Output::destroy($request->ids);
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-deleted-rows')],200);
    }
    


    public function update_status(Request $request)
    {

        $output=Output::find($request->id);
       
        if($output->state==0){
        $output->update(['state'=>1]);
        }else{
        $output->update(['state'=>0]);
        }
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-updated-row')],200);
    }


}
