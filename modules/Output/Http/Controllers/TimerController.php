<?php

namespace Modules\Output\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Output\Entities\Timer;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Support\Renderable;
use Modules\Core\Http\Controllers\AppController;
use Modules\Output\Entities\Output;
use Modules\Output\Entities\OutputDay;
use Modules\Output\Entities\TimerDay;
use Modules\Output\Http\Requests\TimeRequest;

class TimerController extends AppController
{


    public function index()
    {
        $this->setMessages([
            //swal
            'swal-delete-prompt' => Lang::get('core::global.swal.swal-delete-prompt'),
            'swal-delete-prompt-single' => Lang::get('core::global.swal.swal-delete-prompt-single'),
            'swal-hard-delete-prompt' => Lang::get('core::global.swal.swal-hard-delete-prompt'),
            'swal-hard-delete-prompt-single' => Lang::get('core::global.swal.swal-hard-delete-prompt-single'),
            'swal-delete-btn-confirm' => Lang::get('core::global.swal.swal-delete-btn-confirm'),
            'swal-delete-btn-discard' => Lang::get('core::global.swal.swal-delete-btn-discard'),

            'swal-restore-prompt' => Lang::get('core::global.swal.swal-restore-prompt'),
            'swal-restore-prompt-single' => Lang::get('core::global.swal.swal-restore-prompt-single'),
            'swal-restore-btn-confirm' => Lang::get('core::global.swal.swal-restore-btn-confirm'),
            'swal-restore-btn-discard' => Lang::get('core::global.swal.swal-restore-btn-discard'),
        ]);
        return view('output::dashboard.timer.index');
    }

        
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $this->setAjaxParams([
            'dt_modal_request_type' => 'POST',
            'dt_modal_submit_url' => route('timer.store')
        ]);
        $output=Output::all();
        return view('output::dashboard.timer.modals.add')->with('output',$output);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(TimeRequest $request)
    {
        $timer=Timer::create([
            'output_id'=>$request->output,
            'start_time'=>$request->start_time,
            'end_time'=>$request->end_time,            
        ]);

        foreach($request->days as $days){
            TimerDay::create([
                'timer_id'=>$timer->id,
                'day'=>$days
            ]);
        }

        return response()->json(['message' => Lang::get('core::global.toastr.toastr-added-row')],201);
    }

    


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Timer $timer)
    {
        $this->setAjaxParams([
            'dt_modal_request_type' => 'PUT',
            'dt_modal_submit_url' => route('timer.update', [$timer->id]),
        ]);
        $output=Output::all();
          return view('output::dashboard.timer.modals.edit')->with(['timer'=>$timer,'output'=>$output]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(TimeRequest $request, Timer $timer)
    {
       
        $timer->update([
            'output_id'=>$request->output,
            'start_time'=>$request->start_time,
            'end_time'=>$request->end_time, 
        ]);

        

        // timer days

        TimerDay::where(['timer_id'=>$timer->id])->delete();

        foreach($request->days as $days){
            TimerDay::create([
                'timer_id'=>$timer->id,
                'day'=>$days
            ]);
        }


        return response()->json(['message' => Lang::get('core::global.toastr.toastr-updated-row')],200);
    }


    public function destroy(Timer $timer)
    {
        $timer->delete();
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-deleted-row')], 200);
    }


    public function destroyMany(Request $request){
        Timer::destroy($request->ids);
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-deleted-rows')],200);
    }

}
