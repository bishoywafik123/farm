<?php

namespace Modules\Output\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateOutputRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => ['required',Rule::unique('outputs','name')->ignore($this->output)],
            'device' => ['required'],
            'gpio' => 'required',
            'status' => 'nullable',
            'active'=>'required' // manual or timer
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
