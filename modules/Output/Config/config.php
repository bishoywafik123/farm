<?php

use Illuminate\Support\Facades\Lang;

return [
    'name' => 'Output',
    'menus' => [
        'back_menus' => [ // support many menus per module
            'output' => [
                'title' => Lang::get('output::menus.main_title'),
                'icon' => 'fas fa-laptop-house',
                'order' => 5,
                'permissions' => ['output.actions.view'], // here you put all sub items permissions
                'sub_menu' => [
                    'item_1' => [
                        'title' => Lang::get('output::menus.sub_title_1'),
                        'route' => 'output.index',
                        'permissions' => 'output.actions.view',
                    ],
                    'item_2' => [
                        'title' => Lang::get('output::menus.sub_title_2'),
                        'route' => 'timer.index',
                        'permissions' => 'timer.actions.view',
                    ]
                ]
                
            ],


        ]
    ],
    'permissions' => [
        'resources' => [
            'output' => [
                'actions' => [
                    'view','edit','delete','export'
                ],
            ],
            
            'timer' => [
                'actions' => [
                    'view','edit','delete','export'
                ],
            ],
        ]
    ]
];
