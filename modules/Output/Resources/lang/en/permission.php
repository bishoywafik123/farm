<?php // this structure is very important as it influences the results of the autocomplete directly
return[
    'resources' => [
        'output' => [
            'title' => 'output', // main title in the autocomplete for this module
            'actions' => [
                'title' => 'Actions',
                'view' => [
                    'title' => 'View',
                    'description' => 'Allow view all outputs'
                ],
                'add' => [
                    'title' => 'Add',
                    'description' => 'Allow add all outputs'
                ],        
                'edit' => [
                    'title' => 'Edit',
                    'description' => 'Allow edit all outputs'
                ],        
                'delete' => [
                    'title' => 'delete',
                    'description' => 'Allow delete all outputs'
                ],
                'export' => [
                    'title' => 'export',
                    'description' => 'Allow export all outputs'
                ],
               
            ],
        ],


        'timer' => [
            'title' => 'Timers', // main title in the autocomplete for this module
            'actions' => [
                'title' => 'Actions',
                'view' => [
                    'title' => 'View',
                    'description' => 'Allow view all timers'
                ],
                'add' => [
                    'title' => 'Add',
                    'description' => 'Allow add all timers'
                ],        
                'edit' => [
                    'title' => 'Edit',
                    'description' => 'Allow edit all timers'
                ],        
                'delete' => [
                    'title' => 'delete',
                    'description' => 'Allow delete all timers'
                ],
                'export' => [
                    'title' => 'export',
                    'description' => 'Allow export all timers'
                ],
               
            ],
        ],



    ],
    
];