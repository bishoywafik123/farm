<?php
return [
    'add-title' => 'Add output',
    'edit-title' => 'Edit output',

    'add-title_2' => 'Add Timer',
    'edit-title_2' => 'Edit Timer',

    'form' => [
        'submit' => 'submit',
        'cancel' => 'cancel',
        'loading_submit' => 'Loading',
        'login-section' => 'Informations Login',
        'name-label' => 'Name',
         "gpio" => "Gpio",
         "status" => "Status",
         "active" => "Active",
         "select device" => "Select device",


         // timer form
         "select output" => "Select output",
         "start time" => "Start time",
         "end time" => "End time",
         "select days"=>"Select Days",

        'errors' => 'Errors',
    ]
];