<?php
 return [
     'columns' => [
        //output 
         "name" => "Name",
         "device" => "device",
         "gpio" => "gpio",
         "state" => "status",



         // timer 
         "output" => "Output",
         'start time'=>'Start time',
         'end time'=>'End time',

         "creation_date" => "Date",
     ],
];