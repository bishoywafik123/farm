<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'dashboard', 'middleware' => 'auth:dashboard'], function(){

    // rest of routes
    Route::group(['prefix' => 'devices', 'as' => 'device.'], function(){

        //the grouped operations (declare them before resource routes to avoid conflicts)
        Route::delete('delete-many', [Modules\Device\Http\Controllers\DeviceController::class, 'destroyMany'])
        ->name('destroy.many')
        ->middleware('can:device.actions.delete');

        Route::get('ajax/datatable', DeviceDatatableController::class)
            ->name('datatable')
            ->middleware('can:device.actions.view');

        //Model Resources routes
        //didn't use resource to have the ability to attach permission middleware to each route
        // avoid using middlewares inside of controllers keep this structure maintained
        Route::get('/',[Modules\Device\Http\Controllers\DeviceController::class, 'index'])
            ->name('index')
            ->middleware('can:device.actions.view');

        Route::post('/',[Modules\Device\Http\Controllers\DeviceController::class, 'store'])
            ->name('store')
            ->middleware('can:device.actions.add');

        Route::get('create',[Modules\Device\Http\Controllers\DeviceController::class, 'create'])
            ->name('create')
            ->middleware('can:device.actions.add');

        Route::put('{device}',[Modules\Device\Http\Controllers\DeviceController::class, 'update'])
            ->name('update')
            ->middleware('can:device.actions.edit');

        Route::get('{device}/edit',[Modules\Device\Http\Controllers\DeviceController::class, 'edit'])
            ->name('edit')
            ->middleware('can:device.actions.edit');

        Route::delete('{device}',[Modules\Device\Http\Controllers\DeviceController::class, 'destroy'])
            ->name('delete')
            ->middleware('can:device.actions.delete');




    });


});






