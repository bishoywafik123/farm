<?php // this structure is very important as it influences the results of the autocomplete directly
return[
    'resources' => [
        'device' => [
            'title' => 'devices', // main title in the autocomplete for this module
            'actions' => [
                'title' => 'Actions',
                'view' => [
                    'title' => 'View',
                    'description' => 'Allow view all Devices'
                ],
                'add' => [
                    'title' => 'Add',
                    'description' => 'Allow add all Devices'
                ],        
                'edit' => [
                    'title' => 'Edit',
                    'description' => 'Allow edit all Devices'
                ],        
                'delete' => [
                    'title' => 'delete',
                    'description' => 'Allow delete all Devices'
                ],
                'export' => [
                    'title' => 'export',
                    'description' => 'Allow export all Devices'
                ],
               
            ],
        ],
    ],
    
];