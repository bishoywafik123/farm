<?php
return [
    'add-title' => 'Add Device',
    'edit-title' => 'Edit Device',
    'form' => [
        'submit' => 'submit',
        'cancel' => 'cancel',
        'loading_submit' => 'Loading',
        'login-section' => 'Informations Login',
        'name-label' => 'Name',
        'errors' => 'Errors',
    ]
];