<?php

use Illuminate\Support\Facades\Lang;

return [
    'name' => 'device',
    'menus' => [
        'back_menus' => [ // support many menus per module
            'device' => [
                'title' => Lang::get('device::menus.main_title'),
                'icon' => 'fas fa-laptop-house',
                'order' => 5,
                'permissions' => ['device.actions.view'], // here you put all sub items permissions
                'sub_menu' => [
                    'item_1' => [
                        'title' => Lang::get('device::menus.sub_title_1'),
                        'route' => 'device.index',
                        'permissions' => 'device.actions.view',
                    ]
                ]
            ]
        ]
    ],
    'permissions' => [
        'resources' => [
            'device' => [
                'actions' => [
                    'view','add','edit','delete','export'
                ],
            ],
        ]
    ]
];
