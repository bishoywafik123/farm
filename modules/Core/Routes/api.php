<?php

use Illuminate\Http\Request;


Route::group(['middleware' => ['api'],'namespace'=>'Api'], function ($router) {

    
    // get devices
    Route::get('/devices/index', 'DeviceController@index');

    // get outputs
    Route::get('/outputs/index', 'OutputController@index');
    Route::get('/outputs/device_id={device}', 'OutputController@filter_device_id');

    Route::put('/output/store', 'OutputController@store');

    //update status output where gpio
    Route::put('/output/status/update/pio={gpio}', 'OutputController@update_status');


    //store sensors
    Route::put('/sensor/store', 'SensorController@store');



    
});
