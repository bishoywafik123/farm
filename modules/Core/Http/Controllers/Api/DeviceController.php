<?php

namespace Modules\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Device\Entities\Device;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\apiResponseTrait;

class DeviceController extends Controller
{
    //
    use apiResponseTrait;
    public function index(){
        $devices=Device::all();
  
        if(count($devices)>0){
            return $this->apiResponse(['devices'=>$devices],'all devices',200);
        }else{
            return $this->apiResponse(['devices'=>''],'No data available',200);
        }

    }
}
