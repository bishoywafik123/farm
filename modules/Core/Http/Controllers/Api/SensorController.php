<?php

namespace Modules\Core\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Sensor\Entities\Sensor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\apiResponseTrait;

class SensorController extends Controller
{
    //
    
    use apiResponseTrait;
    
    public function store(Request $request){

        $validate=Validator::make($request->all(),[
            'device_id'=>'required',
            'temp_value'=>'required',
            'hum_value'=>'required',
            'soil_hum_value'=>'required',
            'weather_value'=>'required',
        ]);
        if($validate->fails()){
            return $this->apiResponse($validate->errors(),'Sorry, Failed to save Sensor',400);
        }
        
        $sensor=Sensor::create([
            'device_id'=>$request->device_id,
            'tempValue'=>$request->temp_value,
            'humValue'=>$request->hum_value,
            'soilHumValue'=>$request->soil_hum_value,
            'weatherValue'=>$request->weather_value,
        ]);
  
            
        return $this->apiResponse(['sensor'=>$sensor],'sensor created successfully',200);

    }


}
