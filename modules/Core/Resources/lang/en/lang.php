<?php
return [
    'error_occured' => 'une erreur s\'est produite',
    'dt_pagination' => [
        'page' => 'Page',
        'of' => 'of',
        'first' => 'first',
        'previous' => 'previous',
        'next' => 'next',
        'last' => 'last',
    ],
    'Messages'=>'Messages',
];