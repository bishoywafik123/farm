<?php
return [
    'datatable' => [
        'header' => [
            'search' => 'search',
            'add' => 'add',
            'trash' => 'trash',
            'filter_title' => 'filter_title',
            'tools' => 'tools',
            'columns' => 'columns',
            'refresh' => 'refresh',
            'fullscreen' => 'fullscreen',
            'selected' => 'selected',
            'delete' => 'delete',
            'restore' => 'restore',
            'actions' => 'Actions',


            'disable actualisation dossier'=>'désactiver actualisation dossier',
            'enable actualisation dossier'=>'permettre actualisation dossier',
        ],
        'columns' => [
            'id' => 'ID',
            "actions" => "Actions"
        ],
        'actions' => [
            'edit' => 'edit',
            'delete' => 'delete',
            'restore' => 'restore',
            'view'=>'view',
            'download'=>'download',
            'confirm'=>'confirm',
            'cancel'=>'cancel',
            'active'=>'Active',
            'block'=>'block',
        ],
        'pagination' => [
            'page' => 'Page',
            'of' => 'of',
            'first' => 'first',
            'previous' => 'previous',
            'next' => 'next',
            'last' => 'last',
        ]
    ],
    'swal' => [
        "swal-form-success" => "The form has been submitted successfully!",
        "swal-export-success" => "The table has been exported successfully!",
        "swal-btn-confirm" => "Ok, successful !",
        "swal-error" => "Sorry, it looks like there are errors detected, please try again.",
    
        "swal-cancel-prompt" => "Are you sure you want to cancel?",
        "swal-cancel-btn-confirm" => "Ok cancel !",
        "swal-cancel-btn-discard" => "No comeback",
        "swal-cancel-discarded" => "Your form has not been canceled !",
        
        "swal-delete-prompt" => "Are you sure you want to delete the selected items?",
        "swal-hard-delete-prompt" => "Are you sure you want to permanently delete the selected items?",
        "swal-delete-prompt-single" => "Are you sure you want to delete this",
        "swal-hard-delete-prompt-single" => "Are you sure you want to permanently delete this",
        "swal-delete-btn-confirm" => "Yes, delete !",
        "swal-delete-btn-discard" => "No cancel",
        "swal-delete-confirmed" => "You have deleted all selected items !",
        "swal-delete-confirmed-single" => "You have deleted the selected item !",
        "swal-delete-discarded" => "Selected items have not been deleted.",
        "swal-delete-discarded-single" => "The selected item has not been deleted.",
    


        
        "swal-update-prompt" => "Êtes-vous sûr de vouloir mettre à jour les éléments sélectionnés ?",
        "swal-update-prompt-single" => "Êtes-vous sûr de vouloir mettre à jour cet élément ?",
        "swal-update-btn-confirm" => "Oui, mettre à jour !",
        "swal-update-btn-discard" => "Non, annuler",
        "swal-update-confirmed" => "Vous avez mis à jour tous les éléments sélectionnés !",
        "swal-update-confirmed-single" => "Vous avez mis à jour l'élément sélectionné !",
        "swal-update-discarded" => "Les éléments sélectionnés n'ont pas été mis à jour.",
        "swal-update-discarded-single" => "L'élément sélectionné n'a pas été mis à jour.",
    
    
        "swal-restore-prompt" => "Êtes-vous sûr de vouloir restaurer les éléments sélectionnés ?",
        "swal-restore-prompt-single" => "Êtes-vous sûr de vouloir restaurer cet élément ?",
        "swal-restore-btn-confirm" => "Oui, Restaurer !",
        "swal-restore-btn-discard" => "Non, annuler",
        "swal-restore-confirmed" => "Vous avez restauré tous les éléments sélectionnés !",
        "swal-restore-confirmed-single" => "Vous avez restauré l'élément sélectionné !",
        "swal-restore-discarded" => "Les éléments sélectionnés n'ont pas été restauré.",
        "swal-restore-discarded-single" => "L'élément sélectionné n'a pas restauré.",
    ],
    'toastr' => [
        'toastr-deleted-row' => 'supprimé avec succés',
        'toastr-hard-deleted-row' => 'supprimé définitivement avec succés',
        'toastr-deleted-rows' => ' supprimés avec succés',
        'toastr-hard-deleted-rows' => ' supprimés définitivement avec succés',
        'toastr-updated-row' => 'mise à jour avec succés',
        'toastr-updated-rows' => ' mise à jour avec succés',
        'toastr-added-row' => 'ajouté avec succés',
        'toastr-restored-row' => 'restauré avec succés',
        'toastr-restored-rows' => ' restaurés avec succés',
        'toastr-send-notification'=>"La notification a été envoyée avec succès",
        'toastr-restored-rows' => ' restaurés avec succés',
        'error_occured' => 'une erreur s\'est produite',
    ],

];