<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Core\Entities\Option;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique();
            $table->json('value')->nullable();
        });
        // Set some keys for the application
        Option::setKeyValue([
            "app_name" => "cloaoran",
            "app_description" => "L’Ordre National des Architectes est un organisme officiel. Il est placé auprès du Ministère chargé de l’architecture, actuellement Ministère de l’Habitat de l’Urbanisme et de la Ville (MHUV).",
            "languages" => ["fr" => "français","ar" => "العربية"],
            "default_language" => "fr",
            "address" => "Centre d’affaire El-Mountazeh Rue Harouni Bouziane31000, Oran, Algérie",
            "email" => "contact@cloaoran.com",
            "web_url" => "https://www.cloaoran.dz",
            "phone" => "+213 (0) 657623463",
            "fax" => "67 14 87 04106 57 62 34 63",
            "logo" => url(asset("themes/metro8/assets/media/logo.jfif")),
            "Architect_Agreement_Number_Lenght" => 5,
            "lat"=>'35.701083',
            "lng"=>'-0.631417',
            "logo_pdf" => url(asset("themes/metro8/assets/media/logo.png")),
            "address_pdf" => "Centre El Mountazah -Oran-",
            "reinscription"=>["title"=>"Actualisation dossier 2022","status"=>1],
            "reinscription_title"=>"Actualisation dossier 2022",
            "reinscription_status"=>1

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
