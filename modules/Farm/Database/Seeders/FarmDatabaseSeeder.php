<?php

namespace Modules\Farm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Farm\Entities\Farm;

class FarmDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        $farms=Farm::create([
                'name' => 'farm1',
                'place' => 'marka',
                'phone' => '0795970357',
                'price' => 200,
                'note' => 'lksfvnlkdsnv',
                'email' => 'mutasem@gmail.com',
                'password' => '123456',
                'is_active' => 1,
                'device_id'=>1
            
        ]);

    }
}
