<?php

use Illuminate\Support\Facades\Lang;

return [
    'name' => 'farm',
    'menus' => [
        'back_menus' => [ // support many menus per module
            'device' => [
                'title' => Lang::get('farm::menus.main_title'),
                'icon' => 'fas fa-tractor',
                'order' => 5,
                'permissions' => ['farm.actions.view'], // here you put all sub items permissions
                'sub_menu' => [
                    'item_1' => [
                        'title' => Lang::get('farm::menus.sub_title_1'),
                        'route' => 'farm.index',
                        'permissions' => 'farm.actions.view',
                    ]
                ]
            ]
        ]
    ],
    'permissions' => [
        'resources' => [
            'farm' => [
                'actions' => [
                    'view','add','edit','delete','export'
                ],
            ],
        ]
    ]
];
