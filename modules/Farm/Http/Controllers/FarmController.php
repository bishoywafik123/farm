<?php

namespace Modules\Farm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Support\Renderable;
use Modules\Core\Http\Controllers\AppController;
use Modules\Device\Entities\Device;
use Modules\Farm\Entities\Farm;
use Modules\Farm\Http\Requests\FarmRequest;
use Modules\Farm\Http\Requests\UpdateFarmRequest;

class FarmController extends AppController
{
    
    public function index()
    {
        $this->setMessages([
            //swal
            'swal-delete-prompt' => Lang::get('core::global.swal.swal-delete-prompt'),
            'swal-delete-prompt-single' => Lang::get('core::global.swal.swal-delete-prompt-single'),
            'swal-hard-delete-prompt' => Lang::get('core::global.swal.swal-hard-delete-prompt'),
            'swal-hard-delete-prompt-single' => Lang::get('core::global.swal.swal-hard-delete-prompt-single'),
            'swal-delete-btn-confirm' => Lang::get('core::global.swal.swal-delete-btn-confirm'),
            'swal-delete-btn-discard' => Lang::get('core::global.swal.swal-delete-btn-discard'),

            'swal-restore-prompt' => Lang::get('core::global.swal.swal-restore-prompt'),
            'swal-restore-prompt-single' => Lang::get('core::global.swal.swal-restore-prompt-single'),
            'swal-restore-btn-confirm' => Lang::get('core::global.swal.swal-restore-btn-confirm'),
            'swal-restore-btn-discard' => Lang::get('core::global.swal.swal-restore-btn-discard'),
        ]);
        return view('farm::dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $this->setAjaxParams([
            'dt_modal_request_type' => 'POST',
            'dt_modal_submit_url' => route('farm.store')
        ]);

        $device=Device::all();
        return view('farm::dashboard.modals.add')->with('device',$device);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(FarmRequest $request)
    {
        if($request->active){
            $active=1;
        }else{
            $active=0;
        }
        $farm=Farm::create([
            'name' => $request->name,
            'place' => $request->place,
            'phone' => $request->phone,
            'price' => $request->price,
            'note' => $request->note,
            'email' => $request->email,
            'password' => $request->password,
            'is_active' => $active,
            'device_id'=>$request->device
        ]);
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-added-row')],201);
    }

    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Farm $farm)
    {
        $this->setAjaxParams([
            'dt_modal_request_type' => 'PUT',
            'dt_modal_submit_url' => route('farm.update', [$farm->id]),
        ]);
        $device=Device::all();

          return view('farm::dashboard.modals.edit')->with(['farm'=>$farm,'device'=>$device]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateFarmRequest $request, Farm $farm)
    {
       
        if($request->active){
            $active=1;
        }else{
            $active=0;
        }

        $farm->update([
            'name' => $request->name,
            'place' => $request->place,
            'phone' => $request->phone,
            'price' => $request->price,
            'note' => $request->note,
            'email' => $request->email,
            'password' => $request->password,
            'is_active' => $active,
            'device_id'=>$request->device
        ]);
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-updated-row')],200);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Farm $farm)
    {
        $farm->delete();
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-deleted-row')], 200);
    }


    public function destroyMany(Request $request){
        Farm::destroy($request->ids);
        return response()->json(['message' => Lang::get('core::global.toastr.toastr-deleted-rows')],200);
    }
    

}
