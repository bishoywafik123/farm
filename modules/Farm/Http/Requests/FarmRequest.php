<?php

namespace Modules\Farm\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FarmRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => ['required','unique:farms'],
            'phone' => 'required',
            'email' => 'required',
            'password' => 'required',
            'place' => 'required',
            'price'=>'required',
            'note'=>'nullable',
            'status'=>'nullable',
            'device'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
