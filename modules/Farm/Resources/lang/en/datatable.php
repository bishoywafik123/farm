<?php
 return [
     'columns' => [
         "name" => "Name",
         "email" => "Email",
         "password" => "Password",
         "phone" => "Phone",
         "place" => "Place",
         "price" => "Price",
         "status" => "Status",
         "creation_date" => "Date",
     ],
];