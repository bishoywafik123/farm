<?php // this structure is very important as it influences the results of the autocomplete directly
return[
    'resources' => [
        'farm' => [
            'title' => 'farm', // main title in the autocomplete for this module
            'actions' => [
                'title' => 'Actions',
                'view' => [
                    'title' => 'View',
                    'description' => 'Allow view all Farms'
                ],
                'add' => [
                    'title' => 'Add',
                    'description' => 'Allow add all Farms'
                ],        
                'edit' => [
                    'title' => 'Edit',
                    'description' => 'Allow edit all Farms'
                ],        
                'delete' => [
                    'title' => 'delete',
                    'description' => 'Allow delete all Farms'
                ],
                'export' => [
                    'title' => 'export',
                    'description' => 'Allow export all Farms'
                ],
               
            ],
        ],
    ],
    
];