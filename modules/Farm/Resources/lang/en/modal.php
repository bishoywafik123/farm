<?php
return [
    'add-title' => 'Add Farm',
    'edit-title' => 'Edit Farm',
    'form' => [
        'submit' => 'submit',
        'cancel' => 'cancel',
        'loading_submit' => 'Loading',
        'login-section' => 'Informations Login',
        'name-label' => 'Name',

         "email" => "Email",
         "password" => "Password",
         "phone" => "Phone",
         "place" => "Place",
         "price" => "Price",
         "note" => "Note",
         "status" => "Status",
         "select device" => "Select device",

        'errors' => 'Errors',
    ]
];