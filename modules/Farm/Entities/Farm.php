<?php

namespace Modules\Farm\Entities;

use Modules\Device\Entities\Device;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Farm extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'place', 'phone','email','device_id', 'password', 'note','price', 'is_active'];
    

    public function device()
    {
        return $this->belongsTo(Device::class,'device_id');
    }
    
    protected static function newFactory()
    {
        return \Modules\Farm\Database\factories\FarmFactory::new();
    }
}
