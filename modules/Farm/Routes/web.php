<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'dashboard', 'middleware' => 'auth:dashboard'], function(){

    // rest of routes
    Route::group(['prefix' => 'farms', 'as' => 'farm.'], function(){

        //the grouped operations (declare them before resource routes to avoid conflicts)
        Route::delete('delete-many', [Modules\Farm\Http\Controllers\FarmController::class, 'destroyMany'])
        ->name('destroy.many')
        ->middleware('can:farm.actions.delete');

        Route::get('ajax/datatable', FarmDatatableController::class)
            ->name('datatable')
            ->middleware('can:farm.actions.view');

        //Model Resources routes
        //didn't use resource to have the ability to attach permission middleware to each route
        // avoid using middlewares inside of controllers keep this structure maintained
        Route::get('/',[Modules\Farm\Http\Controllers\FarmController::class, 'index'])
            ->name('index')
            ->middleware('can:farm.actions.view');

        Route::post('/',[Modules\Farm\Http\Controllers\FarmController::class, 'store'])
            ->name('store')
            ->middleware('can:farm.actions.add');

        Route::get('create',[Modules\Farm\Http\Controllers\FarmController::class, 'create'])
            ->name('create')
            ->middleware('can:farm.actions.add');

        Route::put('{farm}',[Modules\Farm\Http\Controllers\FarmController::class, 'update'])
            ->name('update')
            ->middleware('can:farm.actions.edit');

        Route::get('{farm}/edit',[Modules\Farm\Http\Controllers\FarmController::class, 'edit'])
            ->name('edit')
            ->middleware('can:farm.actions.edit');

        Route::delete('{farm}',[Modules\Farm\Http\Controllers\FarmController::class, 'destroy'])
            ->name('delete')
            ->middleware('can:farm.actions.delete');




    });


});






