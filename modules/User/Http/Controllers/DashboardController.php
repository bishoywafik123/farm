<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Sensor\Entities\Sensor;
use Modules\User\Transformers\TempChart;
use Modules\User\Transformers\SensorChart;
use Illuminate\Contracts\Support\Renderable;
use Modules\Core\Http\Controllers\AppController;

class DashboardController extends AppController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        
        $sensors=Sensor::orderBy('id','DESC')->first();

        
        $days_temp=Sensor::select(\DB::raw("MIN(tempValue) AS min_value, MAX(tempValue) AS max_value, DATE_FORMAT(created_at, '%d') As day"))->whereMonth('created_at', '=', date('m'))->groupBy(DB::raw("DATE_FORMAT(created_at, '%d')"))->get();


        $days_humadity=Sensor::select(\DB::raw("MIN(humValue) AS min_value, MAX(humValue) AS max_value, DATE_FORMAT(created_at, '%d') As day"))->whereMonth('created_at', '=', date('m'))->groupBy(DB::raw("DATE_FORMAT(created_at, '%d')"))->get();

        return view('user::dashboard')->with(['sensors'=>$sensors,'days_temp'=>$days_temp,'days_humadity'=>$days_humadity]);
    }

}
