@extends('dashboard::layout.main')

@section('pageHead')
<title>Dashboard | Sengine</title>
<meta name="description"
content="put description here" />
<meta name="keywords"
content="put key words here" />
@endsection

@section('pageStyle')
<link href="{{ asset('/themes/metro8/assets/plugins/datatables/datatables.bundle.css') }}" rel="stylesheet"
    type="text/css">
@endsection

@section('page_title')
<!--begin::Page title-->
<div data-kt-swapper="true" data-kt-swapper-mode="prepend"
    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_header_nav'}"
    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
    <!--begin::Title-->
    <h1 class="d-flex align-items-center fw-bolder fs-3 my-1 toolbar-main-title-color">Dashboard</h1>
    <!--end::Title-->
</div>
<!--end::Page title-->
@endsection

@section('content')
<div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container-xxl container-full-width">
        <!--begin::Card-->
        <h1>Welcome {{ auth()->user()->name }}</h1>
        <!--end::Card-->

        <br><br>



        <div class="col-md-12 mb-3" style="background: #1e1e2d;">
            <a class="weatherwidget-io" href="https://forecast7.com/en/31d9535d93/amman/" data-label_1="AMMAN" data-icons="Climacons" data-days="5" data-theme="orange" data-baseColor="" data-shadow="#bd5a14" data-textColor="#f6ddbb" data-highColor="#ffffff" data-lowColor="#d6f4fa" data-sunColor="#f3f3f3" data-moonColor="#f3f3f3" data-cloudColor="#f3f3f3" data-rainColor="#d6f4fa" data-snowColor="#f3f3f3">AMMAN WEATHER</a>
            <script>
                !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
            </script>
        </div>
        <div class="row" style="margin-top: 25px;">

        <div class="col-12 col-sm-6 col-md-3" style="margin-bottom: 10px;">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-temperature-low icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Current Temprature</span>
                    <br>
                    <div class="info-box-number">
                    {{$sensors->tempValue}}
                    <small>C</small>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3" style="margin-bottom: 10px;">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-tint icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Current Humadity</span>
                    <br>
                    <div class="info-box-number">{{$sensors->humValue}}</div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-cloud icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Weather Condition</span>
                    <br>
                    <div class="info-box-number">{{$sensors->weatherValue}}</div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-map  icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Soil Moisture</span>
                    <br>
                    <div class="info-box-number">{{$sensors->soilHumValue}}</div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-vials  icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">PH Level</span>
                    <br>
                    <div class="info-box-number">7<small>ph</small></div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-vial  icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Potassium Fertilizer</span>
                    <br>
                    <div class="info-box-number">5<small>%</small></div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-syringe  icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Nitrogen Fertilizer</span>
                    <br>
                    <div class="info-box-number">5<small>%</small></div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-capsules  icon-box"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Phosphorus Fertilizer</span>
                    <br>
                    <div class="info-box-number">30<small>%</small></div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

    </div>


    <div class="row">

        <div class="col-12 col-sm-6">
            <div class="box-chart mb-3">
            <h3 style="text-align: center"> Temprature</h3>
            <!--begin::Chart-->
            <canvas id="temp" class="mh-400px"></canvas>
            <!--end::Chart-->
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="box-chart mb-3">
            <h3 style="text-align: center">Humadity</h3>
            <!--begin::Chart-->
            <canvas id="humadity" class="mh-400px"></canvas>

            <!--end::Chart-->
            </div>
        </div>

     
    </div>

    <div class="row box-chart">
        <h3 style="padding:10px 24px;">Soil Measurments</h3>
<hr>
        <div class="col-12 col-sm-4">
            <div class=" mb-3">
            <!--begin::Chart-->
            <h4 style="text-align: center;padding: 10px;font-size: 15px;">Minerals Measurments</h4>



            <div class="progress-group" style="margin-bottom:13px;">
                PH Level
                <span class="float-right" style="float: right;"><b>160</b>/200</span>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-success" style="width: 80%"></div>
                </div>
            </div>
            <!-- /.progress-group -->

            <div class="progress-group" style="margin-bottom:13px;">
                Potassium Fertilizer
                <span class="float-right" style="float: right;"><b>310</b>/400</span>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-warning" style="width: 75%"></div>
                </div>
            </div>

            <!-- /.progress-group -->
            <div class="progress-group" style="margin-bottom:13px;">
                <span class="progress-text">Nitrogen Fertilizer</span>
                <span class="float-right" style="float: right;"><b>480</b>/800</span>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-info" style="width: 60%"></div>
                </div>
            </div>

            <!-- /.progress-group -->
            <div class="progress-group" style="margin-bottom:13px;">
                Phosphorus Fertilizer
                <span class="float-right" style="float: right;"><b>250</b>/500</span>
                <div class="progress progress-sm">
                    <div class="progress-bar bg-danger" style="width: 50%"></div>
                </div>
            </div>
            <!-- /.progress-group -->
    

            <!--end::Chart-->
            </div>
        </div>
        <div class="col-12 col-sm-8">
            <div class=" mb-3">
            <h4 style="text-align: center;padding: 10px;font-size: 15px;">Soil Moisture (AVG Daily)</h4>
            <!--begin::Chart-->
            <canvas id="soilmoist" class="mh-400px"></canvas>

            <!--end::Chart-->
            </div>
        </div>

     
    </div>

    </div>
    <!--end::Container-->
</div>
@endsection


@section('extras')
<!--begin::Scrolltop-->
@include('dashboard::layout.partials.scrollup')
<!--end::Scrolltop-->
<x-dashboard::dt-modal/>
@endsection

@section('pageScripts')
@javascript(['days_temp'=>$days_temp,'days_humadity'=>$days_humadity])
<script>
var KTWidgets = function () {



    var tempWidget = function() {


    var ctx = document.getElementById('temp');

// Define colors
var primaryColor = KTUtil.getCssVariableValue('--bs-primary');
var dangerColor = KTUtil.getCssVariableValue('--bs-danger');
var successColor = KTUtil.getCssVariableValue('--bs-success');

// Define fonts
var fontFamily = KTUtil.getCssVariableValue('--bs-font-sans-serif');




// Chart labels
const labels =[];

php_to_js['days_temp'].forEach(function(item) {
    console.log(item.day);
    labels.push(item.day);
});




var max_temp=[]

php_to_js['days_temp'].forEach(function(item) {
    max_temp.push(item.max_value);
});

var min_temp=[]

php_to_js['days_temp'].forEach(function(item) {
    min_temp.push(item.min_value);
});

// Chart data
const data = {
    labels: labels,
    datasets: [      
    {
    data: max_temp,
    label: 'max temp',
    backgroundColor:successColor,
    borderWidth:1
    },
    {
    data: min_temp,
    label: 'min temp',
    backgroundColor:dangerColor,
    borderWidth:1
    }

    ],


};

// Chart config
const config = {
    type: 'bar',
     data,

    defaults:{
        global: {
            defaultFont: fontFamily
        }
    }
};

// Init ChartJS -- for more info, please visit: https://www.chartjs.org/docs/latest/
var myChart = new Chart(ctx, config);

    };





    
    var humadityWidget = function() {


var ctx = document.getElementById('humadity');

// Define colors
var primaryColor = KTUtil.getCssVariableValue('--bs-primary');
var dangerColor = KTUtil.getCssVariableValue('--bs-danger');
var successColor = KTUtil.getCssVariableValue('--bs-success');

// Define fonts
var fontFamily = KTUtil.getCssVariableValue('--bs-font-sans-serif');


// humadity
var ctx_humadity = document.getElementById('humadity');



// Chart labels
const labels_humadity =[];

php_to_js['days_humadity'].forEach(function(item) {
    console.log(item.day);
    labels_humadity.push(item.day);
});


var max_humadity=[]

php_to_js['days_humadity'].forEach(function(item) {
    max_humadity.push(item.max_value);
});

var min_humadity=[]

php_to_js['days_humadity'].forEach(function(item) {
    min_humadity.push(item.min_value);
});

// Chart data
const data = {
    labels: labels_humadity,
    datasets: [      
    {
    data: max_humadity,
    label: 'max humadity',
    backgroundColor:successColor,
    borderWidth:1
    },
    {
    data: min_humadity,
    label: 'min humadity',
    backgroundColor:dangerColor,
    borderWidth:1
    }

    ],

};


// Chart config
const config = {
    type: 'bar',
     data,

    defaults:{
        global: {
            defaultFont: fontFamily
        }
    }
};

// Init ChartJS -- for more info, please visit: https://www.chartjs.org/docs/latest/
var myChart = new Chart(ctx, config);


    };




    var soilmoistWidget = function() {


    // Soil Moisture Measures
var ctx = document.getElementById('soilmoist').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['00:00', '1:00', '2:00', '3:00', '4:00', '5:00','6:00', '7:00', '8:00', '9:00', '10:00', '11:00','12:00', '13:00', '14:00', '15:00', '16:00', '17:00','18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
        datasets: [{
            label: 'Temprature History',
            data: [30, 25, 21, 18, 18, 16, 15, 90, 80, 70, 65, 55, 50, 40, 39, 35, 35, 31, 30, 30, 30 , 30, 30, 30 ],
            borderWidth: 1,
            backgroundColor: '#9aa0a2',
            color:'#9aa0a2',
            borderColor: "#bae755",

        }],

    },
    options: {
        maintainAspectRatio: false,
        responsive: true,
        title: {
            display: false,
            text: 'Soil Moisture',
            fontColor: '#fff',
            fontSize: 20,
        },
        legend: {
            display:false,
        },
        scales: {
            xAxes: [{
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }
            }]
          }
        
    }
});


    };


   // Public methods
   return {
        init: function () {
            tempWidget();
            humadityWidget();
            soilmoistWidget();
        }
       
    }
}();




// On document ready
KTUtil.onDOMContentLoaded(function() {
    KTWidgets.init();
});



</script>



@endsection