<?php // this structure is very important as it influences the results of the autocomplete directly
return[
    'resources' => [
        'sensor' => [
            'title' => 'sensor', // main title in the autocomplete for this module
            'actions' => [
                'title' => 'Actions',
                'view' => [
                    'title' => 'View',
                    'description' => 'Allow view all sensors'
                ],
       
                'export' => [
                    'title' => 'export',
                    'description' => 'Allow export all sensors'
                ],
               
            ],
        ],
    ],
    
];