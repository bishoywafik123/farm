<?php
 return [
     'columns' => [
         "temp value" => "temp Value",
         "hum value" => "hum Value",
         "soil hum value" => "soil Hum Value",
         "weather value" => "weather Value",
         "device" => "device",
         
         "creation_date" => "Date",
     ],
];