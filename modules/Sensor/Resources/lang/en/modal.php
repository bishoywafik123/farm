<?php
return [
    'add-title' => 'Add sensor',
    'edit-title' => 'Edit sensor',
    'form' => [
        'submit' => 'submit',
        'cancel' => 'cancel',
        'loading_submit' => 'Loading',
        
        'errors' => 'Errors',
    ]
];