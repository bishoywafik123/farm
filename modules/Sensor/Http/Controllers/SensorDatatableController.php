<?php

namespace Modules\Sensor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Sensor\Entities\Sensor;
use Illuminate\Support\Facades\Lang;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Contracts\Support\Renderable;

class SensorDatatableController extends Controller
{
    public function __invoke(Request $request)
    {
        if (!$request->ajax()) {
            abort(403);
        }
        $model = Sensor::all();
        return DataTables::of($model)
    
                ->addColumn('nb_orders', function($row){
                    return rand(0,10);
                })
                ->addColumn('selectRow', function($row){
                    return $row->id;
                })
        
 
                ->editColumn('device', function($row){
                    return $row->device->name;
                })
        
                ->make(true);

    }
}
