<?php

namespace Modules\Sensor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Support\Renderable;
use Modules\Core\Http\Controllers\AppController;

class SensorController extends AppController
{
    
    public function index()
    {
     
        return view('sensor::dashboard.index');
    }


}
