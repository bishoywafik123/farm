<?php

namespace Modules\Sensor\Entities;

use Modules\Device\Entities\Device;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sensor extends Model
{
    use HasFactory;

    protected $fillable = ['tempValue', 'humValue', 'soilHumValue','weatherValue','device_id'];
    
    public function device()
    {
        return $this->belongsTo(Device::class,'device_id');
    }
    protected static function newFactory()
    {
        return \Modules\Sensor\Database\factories\SensorFactory::new();
    }
}
