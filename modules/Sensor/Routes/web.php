<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'dashboard', 'middleware' => 'auth:dashboard'], function(){

    // rest of routes
    Route::group(['prefix' => 'sensors', 'as' => 'sensor.'], function(){


        Route::get('ajax/datatable', SensorDatatableController::class)
            ->name('datatable')
            ->middleware('can:sensor.actions.view');

        //Model Resources routes
        //didn't use resource to have the ability to attach permission middleware to each route
        // avoid using middlewares inside of controllers keep this structure maintained
        Route::get('/',[Modules\Sensor\Http\Controllers\SensorController::class, 'index'])
            ->name('index')
            ->middleware('can:sensor.actions.view');
    });


});






