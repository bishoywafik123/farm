<?php

use Illuminate\Support\Facades\Lang;

return [
    'name' => 'Sensors',
    'menus' => [
        'back_menus' => [ // support many menus per module
            'sensor' => [
                'title' => Lang::get('sensor::menus.main_title'),
                'icon' => 'fas fa-user-shield',
                'order' => 5,
                'permissions' => ['sensor.actions.view'], // here you put all sub items permissions
                'sub_menu' => [
                    'item_1' => [
                        'title' => Lang::get('sensor::menus.sub_title_1'),
                        'route' => 'sensor.index',
                        'permissions' => 'sensor.actions.view',
                    ]
                ]
                
            ],


        ]
    ],
    'permissions' => [
        'resources' => [
            'sensor' => [
                'actions' => [
                    'view','export'
                ],
            ],

        ]
    ]
];
