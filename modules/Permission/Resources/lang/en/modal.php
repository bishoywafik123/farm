<?php
return [
    'add-title' => 'Ajouter un role',
    'edit-title' => 'Modifier un role',
    'form' => [
        'submit' => 'Enregistrer',
        'cancel' => 'Annuler',
        'loading_submit' => 'Veuillez Patienter',
        'login-section' => 'Informations Login',
        'name_label' => 'Nom du role',
        'permissions_label' => 'Permissions',
        'errors' => 'Erreurs',
    ]
];