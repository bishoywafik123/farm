<?php // this structure is very important as it influences the results of the autocomplete directly
return[
    'resources' => [
        'role' => [
            'title' => 'roles', // main title in the autocomplete for this module
            'actions' => [
                'title' => 'Actions',
                'view' => [
                    'title' => 'lecture',
                    'description' => 'Permet de lire et consulter la liste des roles'
                ],
                'add' => [
                    'title' => 'ajout',
                    'description' => 'Permet d\'ajouter un role'
                ],        
                'edit' => [
                    'title' => 'modification',
                    'description' => 'Permet d\'apporter des modifications aux roles'
                ],        
                'delete' => [
                    'title' => 'suppression',
                    'description' => 'Permet de supprimer les roles'
                ],
                'export' => [
                    'title' => 'export',
                    'description' => 'Permet l\'export la table role'
                ],
            ],
        ],
    ],
    
];