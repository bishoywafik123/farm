<?php return array (
  'app' => 
  array (
    'name' => 'Laravel',
    'env' => 'local',
    'debug' => true,
    'url' => 'http://localhost:8000',
    'asset_url' => NULL,
    'timezone' => 'Africa/Cairo',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'faker_locale' => 'en_US',
    'key' => 'base64:SNKfK03m+dAi1dnfQDzfBEYl95HTRLV91JWX+UJ/Ta0=',
    'cipher' => 'AES-256-CBC',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'App\\Providers\\AppServiceProvider',
      23 => 'App\\Providers\\AuthServiceProvider',
      24 => 'App\\Providers\\EventServiceProvider',
      25 => 'App\\Providers\\RouteServiceProvider',
      26 => 'Meneses\\LaravelMpdf\\LaravelMpdfServiceProvider',
      27 => 'Tymon\\JWTAuth\\Providers\\LaravelServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Arr' => 'Illuminate\\Support\\Arr',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Broadcast' => 'Illuminate\\Support\\Facades\\Broadcast',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'Date' => 'Illuminate\\Support\\Facades\\Date',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Http' => 'Illuminate\\Support\\Facades\\Http',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'Str' => 'Illuminate\\Support\\Str',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'PDF' => 'Meneses\\LaravelMpdf\\Facades\\LaravelMpdf',
    ),
  ),
  'arrays' => 
  array (
    'code_phone' => 
    array (
      44 => 'UK (+44)',
      1 => 'Canada (+1)',
      213 => 'Algeria (+213)',
      376 => 'Andorra (+376)',
      244 => 'Angola (+244)',
      1264 => 'Anguilla (+1264)',
      1268 => 'Antigua & Barbuda (+1268)',
      54 => 'Argentina (+54)',
      374 => 'Armenia (+374)',
      297 => 'Aruba (+297)',
      61 => 'Australia (+61)',
      43 => 'Austria (+43)',
      994 => 'Azerbaijan (+994)',
      1242 => 'Bahamas (+1242)',
      973 => 'Bahrain (+973)',
      880 => 'Bangladesh (+880)',
      1246 => 'Barbados (+1246)',
      375 => 'Belarus (+375)',
      32 => 'Belgium (+32)',
      501 => 'Belize (+501)',
      229 => 'Benin (+229)',
      1441 => 'Bermuda (+1441)',
      975 => 'Bhutan (+975)',
      591 => 'Bolivia (+591)',
      387 => 'Bosnia Herzegovina (+387)',
      267 => 'Botswana (+267)',
      55 => 'Brazil (+55)',
      673 => 'Brunei (+673)',
      359 => 'Bulgaria (+359)',
      226 => 'Burkina Faso (+226)',
      257 => 'Burundi (+257)',
      855 => 'Cambodia (+855)',
      237 => 'Cameroon (+237)',
      238 => 'Cape Verde Islands (+238)',
      1345 => 'Cayman Islands (+1345)',
      236 => 'Central African Republic (+236)',
      56 => 'Chile (+56)',
      86 => 'China (+86)',
      57 => 'Colombia (+57)',
      269 => 'Mayotte (+269)',
      242 => 'Congo (+242)',
      682 => 'Cook Islands (+682)',
      506 => 'Costa Rica (+506)',
      385 => 'Croatia (+385)',
      53 => 'Cuba (+53)',
      90392 => 'Cyprus North (+90392)',
      357 => 'Cyprus South (+357)',
      42 => 'Czech Republic (+42)',
      45 => 'Denmark (+45)',
      253 => 'Djibouti (+253)',
      1809 => 'Dominican Republic (+1809)',
      593 => 'Ecuador (+593)',
      20 => 'Egypt (+20)',
      503 => 'El Salvador (+503)',
      240 => 'Equatorial Guinea (+240)',
      291 => 'Eritrea (+291)',
      372 => 'Estonia (+372)',
      251 => 'Ethiopia (+251)',
      500 => 'Falkland Islands (+500)',
      298 => 'Faroe Islands (+298)',
      679 => 'Fiji (+679)',
      358 => 'Finland (+358)',
      33 => 'France (+33)',
      594 => 'French Guiana (+594)',
      689 => 'French Polynesia (+689)',
      241 => 'Gabon (+241)',
      220 => 'Gambia (+220)',
      7880 => 'Georgia (+7880)',
      49 => 'Germany (+49)',
      233 => 'Ghana (+233)',
      350 => 'Gibraltar (+350)',
      30 => 'Greece (+30)',
      299 => 'Greenland (+299)',
      1473 => 'Grenada (+1473)',
      590 => 'Guadeloupe (+590)',
      671 => 'Guam (+671)',
      502 => 'Guatemala (+502)',
      224 => 'Guinea (+224)',
      245 => 'Guinea - Bissau (+245)',
      592 => 'Guyana (+592)',
      509 => 'Haiti (+509)',
      504 => 'Honduras (+504)',
      852 => 'Hong Kong (+852)',
      36 => 'Hungary (+36)',
      354 => 'Iceland (+354)',
      91 => 'India (+91)',
      62 => 'Indonesia (+62)',
      98 => 'Iran (+98)',
      964 => 'Iraq (+964)',
      353 => 'Ireland (+353)',
      39 => 'Italy (+39)',
      1876 => 'Jamaica (+1876)',
      81 => 'Japan (+81)',
      962 => 'Jordan (+962)',
      7 => 'Uzbekistan (+7)',
      254 => 'Kenya (+254)',
      686 => 'Kiribati (+686)',
      850 => 'Korea North (+850)',
      82 => 'Korea South (+82)',
      965 => 'Kuwait (+965)',
      996 => 'Kyrgyzstan (+996)',
      856 => 'Laos (+856)',
      371 => 'Latvia (+371)',
      961 => 'Lebanon (+961)',
      266 => 'Lesotho (+266)',
      231 => 'Liberia (+231)',
      218 => 'Libya (+218)',
      417 => 'Liechtenstein (+417)',
      370 => 'Lithuania (+370)',
      352 => 'Luxembourg (+352)',
      853 => 'Macao (+853)',
      389 => 'Macedonia (+389)',
      261 => 'Madagascar (+261)',
      265 => 'Malawi (+265)',
      60 => 'Malaysia (+60)',
      960 => 'Maldives (+960)',
      223 => 'Mali (+223)',
      356 => 'Malta (+356)',
      692 => 'Marshall Islands (+692)',
      596 => 'Martinique (+596)',
      222 => 'Mauritania (+222)',
      52 => 'Mexico (+52)',
      691 => 'Micronesia (+691)',
      373 => 'Moldova (+373)',
      377 => 'Monaco (+377)',
      976 => 'Mongolia (+976)',
      1664 => 'Montserrat (+1664)',
      212 => 'Morocco (+212)',
      258 => 'Mozambique (+258)',
      95 => 'Myanmar (+95)',
      264 => 'Namibia (+264)',
      674 => 'Nauru (+674)',
      977 => 'Nepal (+977)',
      31 => 'Netherlands (+31)',
      687 => 'New Caledonia (+687)',
      64 => 'New Zealand (+64)',
      505 => 'Nicaragua (+505)',
      227 => 'Niger (+227)',
      234 => 'Nigeria (+234)',
      683 => 'Niue (+683)',
      672 => 'Norfolk Islands (+672)',
      670 => 'Northern Marianas (+670)',
      47 => 'Norway (+47)',
      968 => 'Oman (+968)',
      680 => 'Palau (+680)',
      507 => 'Panama (+507)',
      675 => 'Papua New Guinea (+675)',
      595 => 'Paraguay (+595)',
      51 => 'Peru (+51)',
      63 => 'Philippines (+63)',
      48 => 'Poland (+48)',
      351 => 'Portugal (+351)',
      1787 => 'Puerto Rico (+1787)',
      974 => 'Qatar (+974)',
      262 => 'Reunion (+262)',
      40 => 'Romania (+40)',
      250 => 'Rwanda (+250)',
      378 => 'San Marino (+378)',
      239 => 'Sao Tome & Principe (+239)',
      966 => 'Saudi Arabia (+966)',
      221 => 'Senegal (+221)',
      381 => 'Serbia (+381)',
      248 => 'Seychelles (+248)',
      232 => 'Sierra Leone (+232)',
      65 => 'Singapore (+65)',
      421 => 'Slovak Republic (+421)',
      386 => 'Slovenia (+386)',
      677 => 'Solomon Islands (+677)',
      252 => 'Somalia (+252)',
      27 => 'South Africa (+27)',
      34 => 'Spain (+34)',
      94 => 'Sri Lanka (+94)',
      290 => 'St. Helena (+290)',
      1869 => 'St. Kitts (+1869)',
      1758 => 'St. Lucia (+1758)',
      249 => 'Sudan (+249)',
      597 => 'Suriname (+597)',
      268 => 'Swaziland (+268)',
      46 => 'Sweden (+46)',
      41 => 'Switzerland (+41)',
      963 => 'Syria (+963)',
      886 => 'Taiwan (+886)',
      66 => 'Thailand (+66)',
      228 => 'Togo (+228)',
      676 => 'Tonga (+676)',
      1868 => 'Trinidad & Tobago (+1868)',
      216 => 'Tunisia (+216)',
      90 => 'Turkey (+90)',
      993 => 'Turkmenistan (+993)',
      1649 => 'Turks & Caicos Islands (+1649)',
      688 => 'Tuvalu (+688)',
      256 => 'Uganda (+256)',
      380 => 'Ukraine (+380)',
      971 => 'United Arab Emirates (+971)',
      598 => 'Uruguay (+598)',
      678 => 'Vanuatu (+678)',
      379 => 'Vatican City (+379)',
      58 => 'Venezuela (+58)',
      84 => 'Virgin Islands - US (+1340)',
      681 => 'Wallis & Futuna (+681)',
      969 => 'Yemen (North)(+969)',
      967 => 'Yemen (South)(+967)',
      260 => 'Zambia (+260)',
      263 => 'Zimbabwe (+263)',
    ),
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'dashboard',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'dashboard' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'api' => 
      array (
        'driver' => 'jwt',
        'provider' => 'customers',
        'hash' => false,
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'Modules\\User\\Entities\\User',
      ),
      'customers' => 
      array (
        'driver' => 'eloquent',
        'model' => 'Modules\\Customer\\Entities\\Customer',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
        'throttle' => 60,
      ),
    ),
    'password_timeout' => 10800,
  ),
  'blade-javascript' => 
  array (
    'namespace' => 'php_to_js',
  ),
  'broadcasting' => 
  array (
    'default' => 'log',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '',
        'secret' => '',
        'app_id' => '',
        'options' => 
        array (
          'cluster' => 'mt1',
          'useTLS' => true,
        ),
      ),
      'ably' => 
      array (
        'driver' => 'ably',
        'key' => NULL,
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
        'serialize' => false,
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
        'lock_connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => 'F:\\farm project\\system\\storage\\framework/cache/data',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'cache',
        'lock_connection' => 'default',
      ),
      'dynamodb' => 
      array (
        'driver' => 'dynamodb',
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
        'table' => 'cache',
        'endpoint' => NULL,
      ),
      'octane' => 
      array (
        'driver' => 'octane',
      ),
    ),
    'prefix' => 'laravel_cache',
  ),
  'cors' => 
  array (
    'paths' => 
    array (
      0 => 'api/*',
      1 => 'sanctum/csrf-cookie',
    ),
    'allowed_methods' => 
    array (
      0 => '*',
    ),
    'allowed_origins' => 
    array (
      0 => '*',
    ),
    'allowed_origins_patterns' => 
    array (
    ),
    'allowed_headers' => 
    array (
      0 => '*',
    ),
    'exposed_headers' => 
    array (
    ),
    'max_age' => 0,
    'supports_credentials' => false,
  ),
  'database' => 
  array (
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'url' => NULL,
        'database' => 'farm',
        'prefix' => '',
        'foreign_key_constraints' => true,
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'farm',
        'username' => 'root',
        'password' => '',
        'unix_socket' => '',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => false,
        'engine' => NULL,
        'options' => 
        array (
        ),
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'farm',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
        'prefix_indexes' => true,
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'url' => NULL,
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'farm',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
        'prefix_indexes' => true,
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'client' => 'phpredis',
      'options' => 
      array (
        'cluster' => 'redis',
        'prefix' => 'laravel_database_',
      ),
      'default' => 
      array (
        'url' => NULL,
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => '0',
      ),
      'cache' => 
      array (
        'url' => NULL,
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => '1',
      ),
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => 'F:\\farm project\\system\\storage\\app',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => 'F:\\farm project\\system\\storage\\app/public',
        'url' => 'http://localhost:8000/storage',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
        'bucket' => '',
        'url' => NULL,
        'endpoint' => NULL,
      ),
    ),
    'links' => 
    array (
      'F:\\farm project\\system\\public\\storage' => 'F:\\farm project\\system\\storage\\app/public',
    ),
  ),
  'hashing' => 
  array (
    'driver' => 'bcrypt',
    'bcrypt' => 
    array (
      'rounds' => 10,
    ),
    'argon' => 
    array (
      'memory' => 1024,
      'threads' => 2,
      'time' => 2,
    ),
  ),
  'jwt' => 
  array (
    'secret' => 'SRgk4rqwXS8y5G2xvMh0bgBhLASfEiNfPclZM7QlHEDSNHg85xxAGF8UD6kq3aTF',
    'keys' => 
    array (
      'public' => NULL,
      'private' => NULL,
      'passphrase' => NULL,
    ),
    'ttl' => NULL,
    'refresh_ttl' => 20160,
    'algo' => 'HS256',
    'required_claims' => 
    array (
      0 => 'iss',
      1 => 'iat',
      2 => 'nbf',
      3 => 'sub',
      4 => 'jti',
    ),
    'persistent_claims' => 
    array (
    ),
    'lock_subject' => true,
    'leeway' => 0,
    'blacklist_enabled' => true,
    'blacklist_grace_period' => 0,
    'decrypt_cookies' => false,
    'providers' => 
    array (
      'jwt' => 'Tymon\\JWTAuth\\Providers\\JWT\\Lcobucci',
      'auth' => 'Tymon\\JWTAuth\\Providers\\Auth\\Illuminate',
      'storage' => 'Tymon\\JWTAuth\\Providers\\Storage\\Illuminate',
    ),
  ),
  'larafirebase' => 
  array (
    'authentication_key' => 'AAAAFhaRilc:APA91bFdkXIeBdWKSatxcfu_zjiKcUKtPMiPJDGwhiAO4uSM5u-LLGcigtNYrJBIPHXDIqgGeu1yqQEVffQO--txYg20kdbBcBno-4nt3tuy86Vzat70mafcdvNOPKiD1RR2BqTTXs73',
  ),
  'logging' => 
  array (
    'default' => 'stack',
    'channels' => 
    array (
      'stack' => 
      array (
        'driver' => 'stack',
        'channels' => 
        array (
          0 => 'single',
        ),
        'ignore_exceptions' => false,
      ),
      'single' => 
      array (
        'driver' => 'single',
        'path' => 'F:\\farm project\\system\\storage\\logs/laravel.log',
        'level' => 'debug',
      ),
      'daily' => 
      array (
        'driver' => 'daily',
        'path' => 'F:\\farm project\\system\\storage\\logs/laravel.log',
        'level' => 'debug',
        'days' => 14,
      ),
      'slack' => 
      array (
        'driver' => 'slack',
        'url' => NULL,
        'username' => 'Laravel Log',
        'emoji' => ':boom:',
        'level' => 'debug',
      ),
      'papertrail' => 
      array (
        'driver' => 'monolog',
        'level' => 'debug',
        'handler' => 'Monolog\\Handler\\SyslogUdpHandler',
        'handler_with' => 
        array (
          'host' => NULL,
          'port' => NULL,
        ),
      ),
      'stderr' => 
      array (
        'driver' => 'monolog',
        'level' => 'debug',
        'handler' => 'Monolog\\Handler\\StreamHandler',
        'formatter' => NULL,
        'with' => 
        array (
          'stream' => 'php://stderr',
        ),
      ),
      'syslog' => 
      array (
        'driver' => 'syslog',
        'level' => 'debug',
      ),
      'errorlog' => 
      array (
        'driver' => 'errorlog',
        'level' => 'debug',
      ),
      'null' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\NullHandler',
      ),
      'emergency' => 
      array (
        'path' => 'F:\\farm project\\system\\storage\\logs/laravel.log',
      ),
    ),
  ),
  'mail' => 
  array (
    'default' => 'smtp',
    'mailers' => 
    array (
      'smtp' => 
      array (
        'transport' => 'smtp',
        'host' => 'mailhog',
        'port' => '1025',
        'encryption' => NULL,
        'username' => NULL,
        'password' => NULL,
        'timeout' => NULL,
        'auth_mode' => NULL,
      ),
      'ses' => 
      array (
        'transport' => 'ses',
      ),
      'mailgun' => 
      array (
        'transport' => 'mailgun',
      ),
      'postmark' => 
      array (
        'transport' => 'postmark',
      ),
      'sendmail' => 
      array (
        'transport' => 'sendmail',
        'path' => '/usr/sbin/sendmail -bs',
      ),
      'log' => 
      array (
        'transport' => 'log',
        'channel' => NULL,
      ),
      'array' => 
      array (
        'transport' => 'array',
      ),
    ),
    'from' => 
    array (
      'address' => NULL,
      'name' => 'Laravel',
    ),
    'markdown' => 
    array (
      'theme' => 'default',
      'paths' => 
      array (
        0 => 'F:\\farm project\\system\\resources\\views/vendor/mail',
      ),
    ),
  ),
  'modules' => 
  array (
    'namespace' => 'Modules',
    'stubs' => 
    array (
      'enabled' => false,
      'path' => 'F:\\farm project\\system/vendor/nwidart/laravel-modules/src/Commands/stubs',
      'files' => 
      array (
        'routes/web' => 'Routes/web.php',
        'routes/api' => 'Routes/api.php',
        'views/index' => 'Resources/views/index.blade.php',
        'scaffold/config' => 'Config/config.php',
        'composer' => 'composer.json',
        'assets/js/app' => 'Resources/assets/js/app.js',
        'assets/sass/app' => 'Resources/assets/sass/app.scss',
        'webpack' => 'webpack.mix.js',
        'package' => 'package.json',
      ),
      'replacements' => 
      array (
        'routes/web' => 
        array (
          0 => 'LOWER_NAME',
          1 => 'STUDLY_NAME',
        ),
        'routes/api' => 
        array (
          0 => 'LOWER_NAME',
        ),
        'webpack' => 
        array (
          0 => 'LOWER_NAME',
        ),
        'json' => 
        array (
          0 => 'LOWER_NAME',
          1 => 'STUDLY_NAME',
          2 => 'MODULE_NAMESPACE',
          3 => 'PROVIDER_NAMESPACE',
        ),
        'views/index' => 
        array (
          0 => 'LOWER_NAME',
        ),
        'views/master' => 
        array (
          0 => 'LOWER_NAME',
          1 => 'STUDLY_NAME',
        ),
        'scaffold/config' => 
        array (
          0 => 'STUDLY_NAME',
        ),
        'composer' => 
        array (
          0 => 'LOWER_NAME',
          1 => 'STUDLY_NAME',
          2 => 'VENDOR',
          3 => 'AUTHOR_NAME',
          4 => 'AUTHOR_EMAIL',
          5 => 'MODULE_NAMESPACE',
          6 => 'PROVIDER_NAMESPACE',
        ),
      ),
      'gitkeep' => true,
    ),
    'paths' => 
    array (
      'modules' => 'F:\\farm project\\system\\../modules',
      'assets' => 'F:\\farm project\\system\\../themes',
      'migration' => 'F:\\farm project\\system\\database/migrations',
      'generator' => 
      array (
        'config' => 
        array (
          'path' => 'Config',
          'generate' => true,
        ),
        'command' => 
        array (
          'path' => 'Console',
          'generate' => true,
        ),
        'migration' => 
        array (
          'path' => 'Database/Migrations',
          'generate' => true,
        ),
        'seeder' => 
        array (
          'path' => 'Database/Seeders',
          'generate' => true,
        ),
        'factory' => 
        array (
          'path' => 'Database/factories',
          'generate' => true,
        ),
        'model' => 
        array (
          'path' => 'Entities',
          'generate' => true,
        ),
        'routes' => 
        array (
          'path' => 'Routes',
          'generate' => true,
        ),
        'controller' => 
        array (
          'path' => 'Http/Controllers',
          'generate' => true,
        ),
        'filter' => 
        array (
          'path' => 'Http/Middleware',
          'generate' => true,
        ),
        'request' => 
        array (
          'path' => 'Http/Requests',
          'generate' => true,
        ),
        'provider' => 
        array (
          'path' => 'Providers',
          'generate' => true,
        ),
        'assets' => 
        array (
          'path' => 'Resources/assets',
          'generate' => true,
        ),
        'lang' => 
        array (
          'path' => 'Resources/lang',
          'generate' => true,
        ),
        'views' => 
        array (
          'path' => 'Resources/views',
          'generate' => true,
        ),
        'test' => 
        array (
          'path' => 'Tests/Unit',
          'generate' => true,
        ),
        'test-feature' => 
        array (
          'path' => 'Tests/Feature',
          'generate' => true,
        ),
        'repository' => 
        array (
          'path' => 'Repositories',
          'generate' => false,
        ),
        'event' => 
        array (
          'path' => 'Events',
          'generate' => false,
        ),
        'listener' => 
        array (
          'path' => 'Listeners',
          'generate' => false,
        ),
        'policies' => 
        array (
          'path' => 'Policies',
          'generate' => false,
        ),
        'rules' => 
        array (
          'path' => 'Rules',
          'generate' => false,
        ),
        'jobs' => 
        array (
          'path' => 'Jobs',
          'generate' => false,
        ),
        'emails' => 
        array (
          'path' => 'Emails',
          'generate' => false,
        ),
        'notifications' => 
        array (
          'path' => 'Notifications',
          'generate' => false,
        ),
        'resource' => 
        array (
          'path' => 'Transformers',
          'generate' => false,
        ),
        'component-view' => 
        array (
          'path' => 'Resources/views/components',
          'generate' => false,
        ),
        'component-class' => 
        array (
          'path' => 'View/Component',
          'generate' => false,
        ),
      ),
    ),
    'commands' => 
    array (
      0 => 'CommandMakeCommand',
      1 => 'ControllerMakeCommand',
      2 => 'DisableCommand',
      3 => 'DumpCommand',
      4 => 'EnableCommand',
      5 => 'EventMakeCommand',
      6 => 'JobMakeCommand',
      7 => 'ListenerMakeCommand',
      8 => 'MailMakeCommand',
      9 => 'MiddlewareMakeCommand',
      10 => 'NotificationMakeCommand',
      11 => 'ProviderMakeCommand',
      12 => 'RouteProviderMakeCommand',
      13 => 'InstallCommand',
      14 => 'ListCommand',
      15 => 'ModuleDeleteCommand',
      16 => 'ModuleMakeCommand',
      17 => 'FactoryMakeCommand',
      18 => 'PolicyMakeCommand',
      19 => 'RequestMakeCommand',
      20 => 'RuleMakeCommand',
      21 => 'MigrateCommand',
      22 => 'MigrateRefreshCommand',
      23 => 'MigrateResetCommand',
      24 => 'MigrateRollbackCommand',
      25 => 'MigrateStatusCommand',
      26 => 'MigrationMakeCommand',
      27 => 'ModelMakeCommand',
      28 => 'PublishCommand',
      29 => 'PublishConfigurationCommand',
      30 => 'PublishMigrationCommand',
      31 => 'PublishTranslationCommand',
      32 => 'SeedCommand',
      33 => 'SeedMakeCommand',
      34 => 'SetupCommand',
      35 => 'UnUseCommand',
      36 => 'UpdateCommand',
      37 => 'UseCommand',
      38 => 'ResourceMakeCommand',
      39 => 'TestMakeCommand',
      40 => 'LaravelModulesV6Migrator',
    ),
    'scan' => 
    array (
      'enabled' => false,
      'paths' => 
      array (
        0 => 'F:\\farm project\\system\\vendor/*/*',
      ),
    ),
    'composer' => 
    array (
      'vendor' => 'nwidart',
      'author' => 
      array (
        'name' => 'Nicolas Widart',
        'email' => 'n.widart@gmail.com',
      ),
    ),
    'composer-output' => false,
    'cache' => 
    array (
      'enabled' => false,
      'key' => 'laravel-modules',
      'lifetime' => 60,
    ),
    'register' => 
    array (
      'translations' => true,
      'files' => 'register',
    ),
    'activators' => 
    array (
      'file' => 
      array (
        'class' => 'Nwidart\\Modules\\Activators\\FileActivator',
        'statuses-file' => 'F:\\farm project\\system\\modules_statuses.json',
        'cache-key' => 'activator.installed',
        'cache-lifetime' => 604800,
      ),
    ),
    'activator' => 'file',
  ),
  'permission' => 
  array (
    'name' => 'Permission',
    'menus' => 
    array (
      'back_menus' => 
      array (
        'role' => 
        array (
          'title' => 'Roles',
          'icon' => 'fas fa-pencil',
          'order' => 6,
          'permissions' => 
          array (
            0 => 'role.actions.view',
          ),
          'sub_menu' => 
          array (
            'item_1' => 
            array (
              'title' => 'Liste',
              'route' => 'roles.index',
              'permissions' => 'role.actions.view',
            ),
          ),
        ),
      ),
    ),
    'permissions' => 
    array (
      'resources' => 
      array (
        'role' => 
        array (
          'actions' => 
          array (
            0 => 'view',
            1 => 'add',
            2 => 'edit',
            3 => 'delete',
            4 => 'export',
          ),
        ),
      ),
    ),
    'models' => 
    array (
      'permission' => 'Spatie\\Permission\\Models\\Permission',
      'role' => 'Spatie\\Permission\\Models\\Role',
    ),
    'table_names' => 
    array (
      'roles' => 'roles',
      'permissions' => 'permissions',
      'model_has_permissions' => 'model_has_permissions',
      'model_has_roles' => 'model_has_roles',
      'role_has_permissions' => 'role_has_permissions',
    ),
    'column_names' => 
    array (
      'role_pivot_key' => NULL,
      'permission_pivot_key' => NULL,
      'model_morph_key' => 'model_id',
      'team_foreign_key' => 'team_id',
    ),
    'register_permission_check_method' => true,
    'teams' => false,
    'display_permission_in_exception' => false,
    'display_role_in_exception' => false,
    'enable_wildcard_permission' => false,
    'cache' => 
    array (
      'expiration_time' => 
      DateInterval::__set_state(array(
         'y' => 0,
         'm' => 0,
         'd' => 0,
         'h' => 24,
         'i' => 0,
         's' => 0,
         'f' => 0.0,
         'weekday' => 0,
         'weekday_behavior' => 0,
         'first_last_day_of' => 0,
         'invert' => 0,
         'days' => false,
         'special_type' => 0,
         'special_amount' => 0,
         'have_weekday_relative' => 0,
         'have_special_relative' => 0,
      )),
      'key' => 'spatie.permission.cache',
      'store' => 'default',
    ),
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
        'after_commit' => false,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => 0,
        'after_commit' => false,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => '',
        'secret' => '',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'default',
        'suffix' => NULL,
        'region' => 'us-east-1',
        'after_commit' => false,
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => NULL,
        'after_commit' => false,
      ),
    ),
    'failed' => 
    array (
      'driver' => 'database-uuids',
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
      'endpoint' => 'api.mailgun.net',
    ),
    'postmark' => 
    array (
      'token' => NULL,
    ),
    'ses' => 
    array (
      'key' => '',
      'secret' => '',
      'region' => 'us-east-1',
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => '120',
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => 'F:\\farm project\\system\\storage\\framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'laravel_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => NULL,
    'http_only' => true,
    'same_site' => 'lax',
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => 'F:\\farm project\\system\\resources\\views',
    ),
    'compiled' => 'F:\\farm project\\system\\storage\\framework\\views',
  ),
  'debugbar' => 
  array (
    'enabled' => NULL,
    'except' => 
    array (
      0 => 'telescope*',
      1 => 'horizon*',
    ),
    'storage' => 
    array (
      'enabled' => true,
      'driver' => 'file',
      'path' => 'F:\\farm project\\system\\storage\\debugbar',
      'connection' => NULL,
      'provider' => '',
      'hostname' => '127.0.0.1',
      'port' => 2304,
    ),
    'include_vendors' => true,
    'capture_ajax' => true,
    'add_ajax_timing' => false,
    'error_handler' => false,
    'clockwork' => false,
    'collectors' => 
    array (
      'phpinfo' => true,
      'messages' => true,
      'time' => true,
      'memory' => true,
      'exceptions' => true,
      'log' => true,
      'db' => true,
      'views' => true,
      'route' => true,
      'auth' => false,
      'gate' => true,
      'session' => true,
      'symfony_request' => true,
      'mail' => true,
      'laravel' => false,
      'events' => false,
      'default_request' => false,
      'logs' => false,
      'files' => false,
      'config' => false,
      'cache' => false,
      'models' => true,
      'livewire' => true,
    ),
    'options' => 
    array (
      'auth' => 
      array (
        'show_name' => true,
      ),
      'db' => 
      array (
        'with_params' => true,
        'backtrace' => true,
        'backtrace_exclude_paths' => 
        array (
        ),
        'timeline' => false,
        'duration_background' => true,
        'explain' => 
        array (
          'enabled' => false,
          'types' => 
          array (
            0 => 'SELECT',
          ),
        ),
        'hints' => false,
        'show_copy' => false,
      ),
      'mail' => 
      array (
        'full_log' => false,
      ),
      'views' => 
      array (
        'timeline' => false,
        'data' => false,
      ),
      'route' => 
      array (
        'label' => true,
      ),
      'logs' => 
      array (
        'file' => NULL,
      ),
      'cache' => 
      array (
        'values' => true,
      ),
    ),
    'inject' => true,
    'route_prefix' => '_debugbar',
    'route_domain' => NULL,
    'theme' => 'auto',
    'debug_backtrace_limit' => 50,
  ),
  'pdf' => 
  array (
    'mode' => '',
    'format' => 'A4',
    'default_font_size' => '12',
    'default_font' => 'sans-serif',
    'margin_left' => 10,
    'margin_right' => 10,
    'margin_top' => 10,
    'margin_bottom' => 10,
    'margin_header' => 0,
    'margin_footer' => 0,
    'orientation' => 'P',
    'title' => 'Laravel mPDF',
    'author' => '',
    'watermark' => '',
    'show_watermark' => false,
    'watermark_font' => 'sans-serif',
    'display_mode' => 'fullpage',
    'watermark_text_alpha' => 0.1,
    'custom_font_dir' => '',
    'custom_font_data' => 
    array (
    ),
    'auto_language_detection' => false,
    'temp_dir' => 'C:\\Users\\ENG-BI~1\\AppData\\Local\\Temp',
    'pdfa' => false,
    'pdfaauto' => false,
  ),
  'flare' => 
  array (
    'key' => NULL,
    'reporting' => 
    array (
      'anonymize_ips' => true,
      'collect_git_information' => false,
      'report_queries' => true,
      'maximum_number_of_collected_queries' => 200,
      'report_query_bindings' => true,
      'report_view_data' => true,
      'grouping_type' => NULL,
      'report_logs' => true,
      'maximum_number_of_collected_logs' => 200,
      'censor_request_body_fields' => 
      array (
        0 => 'password',
      ),
    ),
    'send_logs_as_events' => true,
    'censor_request_body_fields' => 
    array (
      0 => 'password',
    ),
  ),
  'ignition' => 
  array (
    'editor' => 'phpstorm',
    'theme' => 'light',
    'enable_share_button' => true,
    'register_commands' => false,
    'ignored_solution_providers' => 
    array (
      0 => 'Facade\\Ignition\\SolutionProviders\\MissingPackageSolutionProvider',
    ),
    'enable_runnable_solutions' => NULL,
    'remote_sites_path' => '',
    'local_sites_path' => '',
    'housekeeping_endpoint_prefix' => '_ignition',
  ),
  'datatables' => 
  array (
    'search' => 
    array (
      'smart' => true,
      'multi_term' => true,
      'case_insensitive' => true,
      'use_wildcards' => false,
      'starts_with' => false,
    ),
    'index_column' => 'DT_RowIndex',
    'engines' => 
    array (
      'eloquent' => 'Yajra\\DataTables\\EloquentDataTable',
      'query' => 'Yajra\\DataTables\\QueryDataTable',
      'collection' => 'Yajra\\DataTables\\CollectionDataTable',
      'resource' => 'Yajra\\DataTables\\ApiResourceDataTable',
    ),
    'builders' => 
    array (
    ),
    'nulls_last_sql' => ':column :direction NULLS LAST',
    'error' => NULL,
    'columns' => 
    array (
      'excess' => 
      array (
        0 => 'rn',
        1 => 'row_num',
      ),
      'escape' => '*',
      'raw' => 
      array (
        0 => 'action',
      ),
      'blacklist' => 
      array (
        0 => 'password',
        1 => 'remember_token',
      ),
      'whitelist' => '*',
    ),
    'json' => 
    array (
      'header' => 
      array (
      ),
      'options' => 0,
    ),
  ),
  'trustedproxy' => 
  array (
    'proxies' => NULL,
    'headers' => 94,
  ),
  'core' => 
  array (
  ),
  'device' => 
  array (
    'name' => 'device',
    'menus' => 
    array (
      'back_menus' => 
      array (
        'device' => 
        array (
          'title' => 'Devices',
          'icon' => 'fas fa-laptop-house',
          'order' => 5,
          'permissions' => 
          array (
            0 => 'device.actions.view',
          ),
          'sub_menu' => 
          array (
            'item_1' => 
            array (
              'title' => 'List',
              'route' => 'device.index',
              'permissions' => 'device.actions.view',
            ),
          ),
        ),
      ),
    ),
    'permissions' => 
    array (
      'resources' => 
      array (
        'device' => 
        array (
          'actions' => 
          array (
            0 => 'view',
            1 => 'add',
            2 => 'edit',
            3 => 'delete',
            4 => 'export',
          ),
        ),
      ),
    ),
  ),
  'farm' => 
  array (
    'name' => 'farm',
    'menus' => 
    array (
      'back_menus' => 
      array (
        'device' => 
        array (
          'title' => 'Users Farms',
          'icon' => 'fas fa-tractor',
          'order' => 5,
          'permissions' => 
          array (
            0 => 'farm.actions.view',
          ),
          'sub_menu' => 
          array (
            'item_1' => 
            array (
              'title' => 'List',
              'route' => 'farm.index',
              'permissions' => 'farm.actions.view',
            ),
          ),
        ),
      ),
    ),
    'permissions' => 
    array (
      'resources' => 
      array (
        'farm' => 
        array (
          'actions' => 
          array (
            0 => 'view',
            1 => 'add',
            2 => 'edit',
            3 => 'delete',
            4 => 'export',
          ),
        ),
      ),
    ),
  ),
  'output' => 
  array (
    'name' => 'Output',
    'menus' => 
    array (
      'back_menus' => 
      array (
        'output' => 
        array (
          'title' => 'Outputs',
          'icon' => 'fas fa-laptop-house',
          'order' => 5,
          'permissions' => 
          array (
            0 => 'output.actions.view',
          ),
          'sub_menu' => 
          array (
            'item_1' => 
            array (
              'title' => 'List',
              'route' => 'output.index',
              'permissions' => 'output.actions.view',
            ),
            'item_2' => 
            array (
              'title' => 'Timer',
              'route' => 'timer.index',
              'permissions' => 'timer.actions.view',
            ),
          ),
        ),
      ),
    ),
    'permissions' => 
    array (
      'resources' => 
      array (
        'output' => 
        array (
          'actions' => 
          array (
            0 => 'view',
            1 => 'edit',
            2 => 'delete',
            3 => 'export',
          ),
        ),
        'timer' => 
        array (
          'actions' => 
          array (
            0 => 'view',
            1 => 'edit',
            2 => 'delete',
            3 => 'export',
          ),
        ),
      ),
    ),
  ),
  'sensor' => 
  array (
    'name' => 'Sensors',
    'menus' => 
    array (
      'back_menus' => 
      array (
        'sensor' => 
        array (
          'title' => 'Read sensors',
          'icon' => 'fas fa-user-shield',
          'order' => 5,
          'permissions' => 
          array (
            0 => 'sensor.actions.view',
          ),
          'sub_menu' => 
          array (
            'item_1' => 
            array (
              'title' => 'List',
              'route' => 'sensor.index',
              'permissions' => 'sensor.actions.view',
            ),
          ),
        ),
      ),
    ),
    'permissions' => 
    array (
      'resources' => 
      array (
        'sensor' => 
        array (
          'actions' => 
          array (
            0 => 'view',
            1 => 'export',
          ),
        ),
      ),
    ),
  ),
  'user' => 
  array (
    'name' => 'User',
    'menus' => 
    array (
      'back_menus' => 
      array (
        'user' => 
        array (
          'title' => 'Users',
          'icon' => 'fas fa-user-shield',
          'order' => 5,
          'permissions' => 
          array (
            0 => 'user.actions.view',
          ),
          'sub_menu' => 
          array (
            'item_1' => 
            array (
              'title' => 'List',
              'route' => 'users.index',
              'permissions' => 'user.actions.view',
            ),
          ),
        ),
      ),
    ),
    'permissions' => 
    array (
      'resources' => 
      array (
        'user' => 
        array (
          'actions' => 
          array (
            0 => 'view',
            1 => 'add',
            2 => 'edit',
            3 => 'delete',
            4 => 'export',
            5 => 'view_trash',
            6 => 'restore',
            7 => 'hard_delete',
          ),
        ),
      ),
    ),
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'alias' => 
    array (
    ),
    'dont_alias' => 
    array (
      0 => 'App\\Nova',
    ),
  ),
);
