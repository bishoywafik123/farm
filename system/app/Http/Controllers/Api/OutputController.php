<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Output\Entities\Output;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\apiResponseTrait;

class OutputController extends Controller
{
    //
    use apiResponseTrait;
    public function index(){
        $outputs=Output::all();
  
        if(count($outputs)>0){
            return $this->apiResponse(['outputs'=>$outputs],'all outputs',200);
        }else{
            return $this->apiResponse(['outputs'=>''],'No data available',200);
        }

    }

    public function filter_device_id($device){
        $outputs=Output::where('device_id',$device)->get();
  
        if(count($outputs)>0){
            return $this->apiResponse(['outputs'=>$outputs],'all outputs by device id',200);
        }else{
            return $this->apiResponse(['outputs'=>''],'No data available',200);
        }

    }


    public function store(Request $request){

        $validate=Validator::make($request->all(),[
            'name'=>'required|unique:outputs',
            'device_id'=>'required',
            'gpio'=>'required',
            'state'=>'required',
        ]);
        if($validate->fails()){
            return $this->apiResponse($validate->errors(),'Sorry, Failed to save output',400);
        }
        
        $output=Output::create([
            'name'=>$request->name,
            'device_id'=>$request->device_id,
            'gpio'=>$request->gpio,
            'state'=>$request->state,
            'active'=>'manual',
        ]);
  
            
        return $this->apiResponse(['output'=>$output],'output created successfully',200);

    }


    

    public function update_status(Request $request,$gpio){

        $validate=Validator::make($request->all(),[
            'status'=>'required',
        ]);
        if($validate->fails()){
            return $this->apiResponse($validate->errors(),'Sorry, Failed to update output status',400);
        }
        
        $output=Output::where('gpio',$gpio)->first();
        if($output){
        $output->update(array('state'=>$request->status));
        $output->save();
        return $this->apiResponse(['output'=>$output],'output status updated successfully',200);
        }else{
            return response()->json('Sorry, Failed to update output status',400);
        }

    }

}
