<?php
namespace App\Http\Controllers\Api;


trait apiResponseTrait{

    public function apiResponse($data='',$message='',$code=200){
        
        $success=['200','201','202'];
        $array=[
          'data'=>$data,  
          'message'=>$message,  
          'status'=>in_array($code,$success) ? true:false,
        ];

        return response($array,$code);
    }

}