<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Output\Entities\Output;

class OutputStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Output:Status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update status output';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

     // update state 1 
        $output_active=Output::whereHas('timers', function($q) {
        $q->where([
            ['start_time','<=' ,date('H:i')],
            ['end_time','>',date('H:i')]
        ])

        ->whereHas('days', function($d) {
            $d->where('day', date('D'));
        });
      })->update(['state'=>1]);


      // update state 0 
      $output_disable=Output::whereHas('timers', function($q) {
        $q->where('start_time','>' ,date('H:i'))->orWhere('end_time','<',date('H:i'))

        ->whereHas('days', function($d){
            $d->where('day', date('D'));
        });

      })->update(['state'=>0]);


    }

    
}
