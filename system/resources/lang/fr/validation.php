<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"         => "Le champ :attribute doit être accepté.",
    "active_url"       => "Le champ :attribute n'est pas une URL valide.",
    "after"            => "Le champ :attribute doit être une date postérieure au :date.",
    "alpha"            => "Le champ :attribute doit seulement contenir des lettres.",
    "alpha_dash"       => "Le champ :attribute doit seulement contenir des lettres, des chiffres et des tirets.",
    "alpha_num"        => "Le champ :attribute doit seulement contenir des chiffres et des lettres.",
    "before"           => "Le champ :attribute doit être une date antérieure au :date.",
    "between"          => [
        "numeric" => "La valeur de :attribute doit être comprise entre :min et :max.",
        "file"    => "Le fichier :attribute doit avoir une taille entre :min et :max kilobytes.",
        "string"  => "Le texte :attribute doit avoir entre :min et :max caractères.",
    ],
    "confirmed"        => "Le champ de confirmation :attribute ne correspond pas.",
    "date"             => "Le champ :attribute n'est pas une date valide.",
    "date_format"      => "Le champ :attribute ne correspond pas au format :format.",
    "different"        => "Les champs :attribute et :other doivent être différents.",
    "digits"           => "Le champ :attribute doit avoir :digits chiffres.",
    "digits_between"   => "Le champ :attribute doit avoir entre :min and :max chiffres.",
    "email"            => "Le format du champ :attribute est invalide.",
    "exists"           => "Le champ :attribute sélectionné est invalide.",
    "image"            => "Le champ :attribute doit être une image.",
    "in"               => "Le champ :attribute est invalide.",
    "integer"          => "Le champ :attribute doit être un entier.",
    "ip"               => "Le champ :attribute doit être une adresse IP valide.",
    "max"              => [
        "numeric" => "La valeur de :attribute ne peut être supérieure à :max.",
        "file"    => "Le fichier :attribute ne peut être plus gros que :max kilobytes.",
        "string"  => "Le texte de :attribute ne peut contenir plus de :max caractères.",
    ],
    "mimes"            => "Le champ :attribute doit être un fichier de type : :values.",
    "min"              => [
        "numeric" => "La valeur de :attribute doit être inférieure à :min.",
        "file"    => "Le fichier :attribute doit être plus que gros que :min kilobytes.",
        "string"  => "Le texte :attribute doit contenir au moins :min caractères.",
    ],
    "not_in"           => "Le champ :attribute sélectionné n'est pas valide.",
    "numeric"          => "Le champ :attribute doit contenir un nombre.",
    "regex"            => "Le format du champ :attribute est invalide.",
    "required"         => "Le champ :attribute est obligatoire.",
    "required_if"      => "Le champ :attribute est obligatoire quand la valeur de :other est :value.",
    "required_with"    => "Le champ :attribute est obligatoire quand :values est présent.",
    "required_without" => "Le champ :attribute est obligatoire quand :values n'est pas présent.",
    "same"             => "Les champs :attribute et :other doivent être identiques.",
    "size"             => [
        "numeric" => "La taille de la valeur de :attribute doit être :size.",
        "file"    => "La taille du fichier de :attribute doit être de :size kilobytes.",
        "string"  => "Le texte de :attribute doit contenir :size caractères.",
    ],
    "unique"           => "La valeur du champ :attribute est déjà utilisée.",
    "url"              => "Le format de l'URL de :attribute n'est pas valide.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */


    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */


    'attributes' => [
        
        //company
        "title_edit" => "Modifier",
        "logo_label" => "Logo",
        "name_label" => "Nom",
        "short_name_label" => "nom court",
        'agreement_date_label'=>'Date de laccord',
        'agreement_place_label'=>'Lieu de laccord',



        //customers

        'select date'=>'sélectionner une date',
        'code phone'=>'coder le téléphone',
        'phone number'=>'numéro de téléphone',
        'date of Birth'=>'Date de naissance',
        'address'=>'Adresse',
        'circle'=>'Daïra',
        'state'=>'Wilaya',
        'zip Code'=>'Code postal',
        'matricule'=>'Matricule',
        'first name'=>'Prénom',
        'last name'=>'Nom',
        'number matricule'=>'N° de matricule',
        'year matricule'=>'Année de matricule',
        'wilaya matricule'=>'Wilaya de Matricule',
        'type matricule'=>"Type d'activité",
        'email address'=>'Email',
        'footer_label'=>'bas de page',


        'title'=>'Titre',
        'body'=>'corps',
        'photo'=>'photo',



        // order

        "Select company"=>"Sélectionnez l'entreprise",
        "First name"=>"Prénom",
        "Last name"=>"Nom",
        "full name"=>"nom et prénom",
        "Vehicle market"=>"Marché des véhicules",
        "Vehicle registration number"=>"Vehicle registration number",

        "date of Birth"=>"DATE DE NAISSANCE",
        "profissional status"=>"STATUT PROFESSIONNEL",
        "place_of_accommodation"=>"Lieu d’hébergement (particulier, hôtel, etc.)",
        "address organization"=>"nom et adresse de l’organisation ou de l’entreprise d’accueil",
        "professional contacts"=>"contacts professionnels ",
        "family status from attached"=>"FICHE FAMILIALE D’ETAT CIVIL JOINTE",

        "address"=>"address",
        "file"=>"file",
        "resoan"=>"raison",
        "yes"=>"Oui",
        "no"=>"Non",
        'title'=>'Title',
        'body'=>'Body',
        'photo'=>'photo',
    ],
];