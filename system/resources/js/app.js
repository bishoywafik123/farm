require('./bootstrap');

const flatpickr = require("flatpickr");
const french = require("flatpickr/dist/l10n/ru.js").default.fr;

flatpickr('.due_date', {
    "locale": french // locale for this instance only
});